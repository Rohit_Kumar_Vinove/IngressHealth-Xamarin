﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.RequestModel
{
    public class SendGivenAnswerRequestModel
    {
        public List<AnswerValues> GivenAnswers {get;set;}
    }

    //public class GivenAnswer
    //{      
    //        public int PageId { get; set; }
    //        public int AnswerId { get; set; }
    //        public string Value { get; set; }      
    //}
}
