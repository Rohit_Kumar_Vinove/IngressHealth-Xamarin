﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.RequestModel
{
  public  class SurveyRequestModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OrderNumber { get; set; }
        public bool Active { get; set; }
       // public DateTime ActiveSince { get; set; }
        // public string Status { get; set; }
    }
}
