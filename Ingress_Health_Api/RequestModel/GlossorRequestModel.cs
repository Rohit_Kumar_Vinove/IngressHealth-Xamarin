﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.RequestModel
{
  public  class GlossorRequestModel
    {
        public int Id { get; set; }
        public string Term { get; set; }
        public string Description { get; set; }
    }
}
