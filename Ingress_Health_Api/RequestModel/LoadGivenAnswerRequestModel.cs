﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.RequestModel
{
   public class LoadGivenAnswerRequestModel
    {        
        public int PageId { get; set; }
        public int AnswerId { get; set; }
        public string Value { get; set; }
    }
}
