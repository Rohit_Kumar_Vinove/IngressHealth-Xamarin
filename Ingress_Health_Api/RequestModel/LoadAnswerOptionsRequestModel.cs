﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.RequestModel
{
   public class LoadAnswerOptionsRequestModel
    {       
            public int Id { get; set; }
            public string Description { get; set; }
            public int? PageId { get; set; }
            public int AnswerType { get; set; }        
    }
}
