﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.RequestModel
{
  public  class SendDiaryEntryRequestModel
    {

        public SendDiaryEntryRequestModel()
        {
            DiaryEntryValues = new List<SendDiaryEntryValue>();
        }
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Header { get; set; }
        public int PageType { get; set; }
        public List<SendDiaryEntryValue> DiaryEntryValues { get; set; }
    }
    public class SendDiaryEntryValue
    {
        public int PageId { get; set; }
        public object Title { get; set; }
        public int Value { get; set; }
    }
}
