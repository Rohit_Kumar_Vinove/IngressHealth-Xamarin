﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
   public class AllSymptomsResponseModel
    {
        public ObservableCollection<symptoms> GetAllSymptoms { get; set; }
    }

    public class symptoms
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
