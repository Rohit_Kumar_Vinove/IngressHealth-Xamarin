﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
   //public class GetDiaryEntryResponseModel
   // {
   // }

    public class DiaryEntryValue
    {
        public int PageId { get; set; }
        public string Title { get; set; }
        public int Value { get; set; }
    }

    public class GetDiaryEntryResponseModel
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string Header { get; set; }
        public int PageType { get; set; }
        public List<DiaryEntryValue> DiaryEntryValues { get; set; }
    }
}
