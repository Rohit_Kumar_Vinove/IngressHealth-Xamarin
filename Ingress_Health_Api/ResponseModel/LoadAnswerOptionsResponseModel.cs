﻿using Ingress_Health_Api.RequestModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
  public  class LoadAnswerOptionsResponseModel: INotifyPropertyChanged
    {
        public ObservableCollection<LoadAnswerOptionsRequestModel> _getAnswersList;

        public ObservableCollection<LoadAnswerOptionsRequestModel> GetAnswersList
        {
            get { return _getAnswersList; }
            set
            {
                _getAnswersList = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;




        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
       
    }
}

