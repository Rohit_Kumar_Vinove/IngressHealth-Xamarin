﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
  public class LoginResponseModel
    {
        public string Username { get; set; }
        public string Token_type { get; set; }
        public string Access_token { get; set; }
        public string Expires_in { get; set; }
        public string issued { get; set; }       
        public string expires { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }
        public string Error_description { get; set; }
    }
    public class EipirationDate
    {
        public static DateTime Expires { get; set; }
    }
}
