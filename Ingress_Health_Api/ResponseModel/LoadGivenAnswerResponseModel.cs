﻿using Ingress_Health_Api.RequestModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
   public class LoadGivenAnswerResponseModel
    {
        public List<LoadGivenAnswerRequestModel> LoadAnswers2DB { get; set; }
    }
}
