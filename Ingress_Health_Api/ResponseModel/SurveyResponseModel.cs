﻿using Ingress_Health_Api.RequestModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
  public  class SurveyResponseModel:INotifyPropertyChanged
    {
        public ObservableCollection<SurveyRequestModel> _getSurveyList;

        public ObservableCollection<SurveyRequestModel> GetSurveyList {
            get {return _getSurveyList; }
            set { _getSurveyList=value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;




        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName=null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        //public int Id { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public int OrderNumber { get; set; }
        // public string Status { get; set; }
    }
}
