﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
   public class GetGraphDataResponseModel
    {
        public GetGraphDataResponseModel()
        {
            GraphDataList = new ObservableCollection<graphdata>();
        }
        //public string Name { get; set; }
        //public ObservableCollection<GraphValue> Values { get; set; }
        public ObservableCollection<graphdata> GraphDataList { get; set; }
    }

    public class graphdata
    {
        public graphdata()
        {
            Values = new ObservableCollection<GraphValue>();
        }
        public string Name { get; set; }
        public ObservableCollection<GraphValue> Values { get; set; }
    }

    public class GraphValue
    {
        public string Date { get; set; }
        public double Value { get; set; }
    }
}
