﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
  

    public class ModelState
    {
        public static object Value { get; internal set; }
        public List<string> errorMessage { get; set; }
    }

    public class RegisterUserResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public ModelState ModelState { get; set; }
    }

}
