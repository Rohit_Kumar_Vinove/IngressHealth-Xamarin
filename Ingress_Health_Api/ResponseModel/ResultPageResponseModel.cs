﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
  public  class ResultPageResponseModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int OrderNumber { get; set; }
        public int PageType { get; set; }
        public int CurrentPageNumber { get; set; }
        public int TotalPages { get; set; }
    }
}
