﻿using Ingress_Health_Api.RequestModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
    public class ShowPackagetoBuyResponse : INotifyPropertyChanged
    {
        public ObservableCollection<ShowPackagetoBuyRequest> _GetPackage2Buy;
        public ObservableCollection<ShowPackagetoBuyRequest> GetPackage2Buy
        {

            get { return _GetPackage2Buy; }
            set
            {
                _GetPackage2Buy = value;
                OnPropertyChanged();
            }

        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
