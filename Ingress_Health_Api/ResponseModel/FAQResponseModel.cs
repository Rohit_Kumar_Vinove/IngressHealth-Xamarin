﻿using Ingress_Health_Api.RequestModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ResponseModel
{
  public  class FAQResponseModel
    {
        private FAQRequestModel _oldproduct;
        public ObservableCollection<FAQRequestModel> GetFAQList { get; set; }
              

        public void HideShowPrpduct(FAQRequestModel product)
        {
            if (_oldproduct == product)
            {
                product.IsVisible = !product.IsVisible;
                UpdateProduct(product);
            }
            else
            {
                if (_oldproduct != null)
                {
                    _oldproduct.IsVisible = false;
                    UpdateProduct(product);
                }
                product.IsVisible = true;
                UpdateProduct(product);
            }
            //product.IsVisible = true;
            //UpdateProduct(product);
            _oldproduct = product;
        }

        private void UpdateProduct(FAQRequestModel product)
        {
            var index = GetFAQList.IndexOf(product);
            GetFAQList.Remove(product);
            GetFAQList.Insert(index,product);
        }
    }

}
