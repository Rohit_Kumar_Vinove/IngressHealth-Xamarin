﻿using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ApiHandler
{
  public  class RestApi
    {       
        public WebRequest webRequest = null;
        private HttpClient client;
        //private int _statusCode;
        //private string _response;
        private string _col = ":";

        public RestApi()

        {

            client = new HttpClient();
         
           
        }

        public async Task<T> GetAsyncData_GetApi<T>(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, T Tobject) where T : new()
        {
            // var _storedToken=Settings;
            try
            {
                client.MaxResponseContentBufferSize = 256000;
                if (IsHeaderRequired)
                {

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objHeaderModel.AccessToken);
                    //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                    //client.DefaultRequestHeaders.Add("Authorization", "application/json");
                    //client.DefaultRequestHeaders.Add("Content-Length", "84");
                    //client.DefaultRequestHeaders.Add("User-Agent", "Fiddler");
                    //client.DefaultRequestHeaders.Add("Host", "localhost:49165");
                    // client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                }
                // var request = new HttpRequestMessage(HttpMethod.Get, uri);

                // request.Content = new FormUrlEncodedContent(keyValues);

                //  HttpResponseMessage response = await client.SendAsync(request);

                HttpResponseMessage response = await client.GetAsync(uri);


                if (response.IsSuccessStatusCode)
                {

                    var responseContent = response.Content;
                    var SucessResponse = await response.Content.ReadAsStringAsync();
                    //  SucessResponse = SucessResponse.Insert(1, "\"Status\"" + _col + "\"Success\",");
                    Tobject = JsonConvert.DeserializeObject<T>(SucessResponse);
                    return Tobject;

                }
                else
                {
                    long ResonseStatus = Convert.ToInt64(response.StatusCode);

                    switch (ResonseStatus)
                    {
                        case 302:
                            return new T();
                            break;
                        case 400:
                            return new T();
                            break;
                        case 401:
                            return new T();
                            break;
                        case 404:
                            return new T();
                            break;

                        default:
                            return new T();
                            break;

                            //long ResonseStatus = Convert.ToInt64(response.StatusCode);
                            //switch (ResonseStatus)
                            //{
                            //    case 302:
                            //        _response = "{\"Status\"" + _col + "\"Invalid User Name and password..\"}";
                            //        break;
                            //    case 400:
                            //        _response = "{\"Status\"" + _col + "\"Bad Request\"}";
                            //        break;
                            //    case 401:
                            //        _response = "{\"Status\"" + _col + "\"Invalid User Name and password..\"}";
                            //        break;
                            //    case 404:
                            //        _response = "{\"Status\"" + _col + "\"Not Found\"}";
                            //        break;

                            //    default:
                            //        _response = "{\"Status\"" + _col + "\"Internal Server errror\"}";
                            //        break;

                            var responseContent = response.Content;
                            var ErrorResponse = await response.Content.ReadAsStringAsync();
                            // ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"fail\",");
                            Tobject = JsonConvert.DeserializeObject<T>(ErrorResponse);
                            return Tobject;

                            // }
                    }

                    //Tobject = JsonConvert.DeserializeObject<T>(_response);
                    //return Tobject;
                }
            }
            catch (Exception ex)
            {

                throw;
                var message = ex.Message;
            }
        }




        //Get Api for List Data
        public async Task<ObservableCollection<T>> GetAsyncData_GetApiList<T>(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, ObservableCollection<T> Tobject) where T : new()
        {
            // var _storedToken=Settings;
            try
            {
                HttpResponseMessage response = null;
              //  client.MaxResponseContentBufferSize = 256000;
                if (IsHeaderRequired)
                {

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objHeaderModel.AccessToken);
                    
                }
               

                 response = await client.GetAsync(uri);
             

                if (response.IsSuccessStatusCode)
                {

                    var responseContent = response.Content;
                    var SucessResponse = await response.Content.ReadAsStringAsync();
                    //  SucessResponse = SucessResponse.Insert(1, "\"Status\"" + _col + "\"Success\",");
                    Tobject = JsonConvert.DeserializeObject<ObservableCollection<T>>(SucessResponse);
                    return Tobject;

                }
                else
                {

                    

                   

                    var responseContent = response.Content;
                    var ErrorResponse = await response.Content.ReadAsStringAsync();
                  //  ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"fail\",");
                    Tobject = JsonConvert.DeserializeObject<ObservableCollection<T>>(ErrorResponse);
                    return Tobject;

                   
                }
              
            }
            catch (Exception ex)
            {

                throw;
                var message = ex.Message;
            }
        }

        public async Task<LoginResponseModel> LoginAsync(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, LoginRequestModel _objLoginrequestmodel)
        {
           
                client.MaxResponseContentBufferSize = 256000;
                LoginResponseModel _objLoginResponseModel;

                var keyValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username",_objLoginrequestmodel.Username),
                new KeyValuePair<string, string>("password",_objLoginrequestmodel.Password),
                new KeyValuePair<string, string>("grant_type",_objLoginrequestmodel.Grant_type)
            };
            if (IsHeaderRequired)
            {
               client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Length", "69");
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Fiddler");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Host", "localhost:49165");
            }
            var request = new HttpRequestMessage(HttpMethod.Post, uri);

                request.Content = new FormUrlEncodedContent(keyValues);

                HttpResponseMessage response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                var SucessResponse = await response.Content.ReadAsStringAsync();
                SucessResponse = SucessResponse.Insert(1, "\"Status\"" + _col + "\"Success\",");
                    _objLoginResponseModel = JsonConvert.DeserializeObject<LoginResponseModel>(SucessResponse);
                JObject jwtDynamic = JsonConvert.DeserializeObject<dynamic>(SucessResponse);
                var AccessTokenExpiration= jwtDynamic.Value<DateTime>(".expires");
                EipirationDate.Expires = AccessTokenExpiration;
                return _objLoginResponseModel;
                }
                else
                {

             
                var ErrorResponse = await response.Content.ReadAsStringAsync();
                ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"Fail\",");
                _objLoginResponseModel = JsonConvert.DeserializeObject<LoginResponseModel>(ErrorResponse);
                return _objLoginResponseModel;
            }                   

        }



        public async Task<RegisterUserResponseModel> RegisterAsync(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, RegisterUserRequestModel _objRegisterUserRequestModel)
        {

            RegisterUserResponseModel objRegisterUserResponseModel;
            string s = JsonConvert.SerializeObject(_objRegisterUserRequestModel);
            HttpResponseMessage response = null;
            using (var stringContent = new StringContent(s, System.Text.Encoding.UTF8, "application/json"))       
            {

                if (IsHeaderRequired)
                {                
                    client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                    client.DefaultRequestHeaders.Add("Content-Length", "84");
                    client.DefaultRequestHeaders.Add("User-Agent", "Fiddler");
                    client.DefaultRequestHeaders.Add("Host", "localhost:49165");
                }
                response = await client.PostAsync(uri, stringContent);

               
                if (response.IsSuccessStatusCode)
                {
                  var  SucessResponse = await response.Content.ReadAsStringAsync();
                    SucessResponse = SucessResponse.Insert(1, "{"+"\"Status\"" + _col + "\"Success\"}");
                    objRegisterUserResponseModel = JsonConvert.DeserializeObject<RegisterUserResponseModel>(SucessResponse);
                    return objRegisterUserResponseModel;
                }
                else
                {
                    var ErrorResponse = await response.Content.ReadAsStringAsync();
                    ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"Fail\",");
                    objRegisterUserResponseModel = JsonConvert.DeserializeObject<RegisterUserResponseModel>(ErrorResponse);                  
                    return objRegisterUserResponseModel;
                }
            }
                               
        }

        public async Task<RegisterUserResponseModel> saveGivenAnswerAsync(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, RegisterUserRequestModel _objRegisterUserRequestModel)
        {

            RegisterUserResponseModel objRegisterUserResponseModel;
            string s = JsonConvert.SerializeObject(_objRegisterUserRequestModel);
            HttpResponseMessage response = null;
            using (var stringContent = new StringContent(s, System.Text.Encoding.UTF8, "application/json"))
            {

                if (IsHeaderRequired)
                {
                    client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                    client.DefaultRequestHeaders.Add("Content-Length", "84");
                    client.DefaultRequestHeaders.Add("User-Agent", "Fiddler");
                    client.DefaultRequestHeaders.Add("Host", "localhost:49165");
                }
                response = await client.PostAsync(uri, stringContent);


                if (response.IsSuccessStatusCode)
                {
                    var SucessResponse = await response.Content.ReadAsStringAsync();
                    SucessResponse = SucessResponse.Insert(1, "{" + "\"Status\"" + _col + "\"Success\"}");
                    objRegisterUserResponseModel = JsonConvert.DeserializeObject<RegisterUserResponseModel>(SucessResponse);
                    return objRegisterUserResponseModel;
                }
                else
                {
                    var ErrorResponse = await response.Content.ReadAsStringAsync();
                    ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"Fail\",");
                    objRegisterUserResponseModel = JsonConvert.DeserializeObject<RegisterUserResponseModel>(ErrorResponse);
                    return objRegisterUserResponseModel;
                }
            }



        }
        public async Task<ResetPasswordResponseModel> ResetPasswordAsync(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, ResetPasswordRequestModel _objResetPasswordRequestModel)
        {

            ResetPasswordResponseModel objResetPasswordResponseModel;
            string s = JsonConvert.SerializeObject(_objResetPasswordRequestModel);
            HttpResponseMessage response = null;
            using (var stringContent = new StringContent(s, System.Text.Encoding.UTF8, "application/json"))
            {

                if (IsHeaderRequired)
                {
                    client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                    client.DefaultRequestHeaders.Add("Content-Length", "84");
                    client.DefaultRequestHeaders.Add("User-Agent", "Fiddler");
                    client.DefaultRequestHeaders.Add("Host", "localhost:49165");
                }
                response = await client.PostAsync(uri, stringContent);


                if (response.IsSuccessStatusCode)
                {
                    var SucessResponse = await response.Content.ReadAsStringAsync();
                     var statuscode =  response.StatusCode;
                   // SucessResponse = SucessResponse.Insert(1, "{" + "\"Status\"" + _col + "\"Success\"}");
                    objResetPasswordResponseModel = JsonConvert.DeserializeObject<ResetPasswordResponseModel>(SucessResponse);
                    return objResetPasswordResponseModel;
                }
                else
                {
                    var ErrorResponse = await response.Content.ReadAsStringAsync();
                   // ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"Fail\",");
                    objResetPasswordResponseModel = JsonConvert.DeserializeObject<ResetPasswordResponseModel>(ErrorResponse);
                    return objResetPasswordResponseModel;
                }
            }
           

        }
        public async Task<ChangeEmailResponseModel> ChangeEmailAsync(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, ChangeEmailRequestModel _objChangeEmailRequestModel)
        {

            ChangeEmailResponseModel objChangeEmailResponseModel;
            string s = JsonConvert.SerializeObject(_objChangeEmailRequestModel);
            HttpResponseMessage response = null;
            using (var stringContent = new StringContent(s, System.Text.Encoding.UTF8, "application/json"))
            {

                if (IsHeaderRequired)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objHeaderModel.AccessToken);
                    //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                    //client.DefaultRequestHeaders.Add("Content-Length", "84");
                    //client.DefaultRequestHeaders.Add("User-Agent", "Fiddler");
                    //client.DefaultRequestHeaders.Add("Host", "localhost:49165");
                }
                response = await client.PostAsync(uri, stringContent);


                if (response.IsSuccessStatusCode)
                {
                    var SucessResponse = await response.Content.ReadAsStringAsync();
                    var statuscode = response.StatusCode;        
                    StatusCodeClass.StatusCode = statuscode.ToString();
                    // SucessResponse = SucessResponse.Insert(1, "{" + "\"Status\"" + _col + "\"Success\"}");
                    objChangeEmailResponseModel = JsonConvert.DeserializeObject<ChangeEmailResponseModel>(SucessResponse);
                    return objChangeEmailResponseModel;
                }
                else
                {
                    var ErrorResponse = await response.Content.ReadAsStringAsync();
                    var statuscode = response.StatusCode;
                    StatusCodeClass.StatusCode = statuscode.ToString();
                    // ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"Fail\",");
                    objChangeEmailResponseModel = JsonConvert.DeserializeObject<ChangeEmailResponseModel>(ErrorResponse);
                    return objChangeEmailResponseModel;
                }
            }

        }
        public async Task<ChangepasswordResponseModel> ChangePasswordAsync(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, ChangePasswordRequestModel _objChangePasswordRequestModel)
        {

            ChangepasswordResponseModel objChangepasswordResponseModel;
            string s = JsonConvert.SerializeObject(_objChangePasswordRequestModel);
            HttpResponseMessage response = null;
            using (var stringContent = new StringContent(s, System.Text.Encoding.UTF8, "application/json"))
            {

                if (IsHeaderRequired)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objHeaderModel.AccessToken);
                   
                }
                response = await client.PostAsync(uri, stringContent);


                if (response.IsSuccessStatusCode)
                {
                    var SucessResponse = await response.Content.ReadAsStringAsync();
                    var statuscode = response.StatusCode;
                    StatusCodeClass.StatusCode = statuscode.ToString();
                    // SucessResponse = SucessResponse.Insert(1, "{" + "\"Status\"" + _col + "\"Success\"}");
                    objChangepasswordResponseModel = JsonConvert.DeserializeObject<ChangepasswordResponseModel>(SucessResponse);
                    return objChangepasswordResponseModel;
                }
                else
                {
                    var ErrorResponse = await response.Content.ReadAsStringAsync();
                    var statuscode = response.StatusCode;
                    StatusCodeClass.StatusCode = statuscode.ToString();
                    // ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"Fail\",");
                    objChangepasswordResponseModel = JsonConvert.DeserializeObject<ChangepasswordResponseModel>(ErrorResponse);
                    return objChangepasswordResponseModel;
                }
            }


        }

        public async Task<SendDiaryEntryResponseModel> SendDiaryEntryAsync(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, SendDiaryEntryRequestModel _objSendDiaryEntryRequestModel)
        {

            SendDiaryEntryResponseModel objSendDiaryEntryResponseModel;
            string s = JsonConvert.SerializeObject(_objSendDiaryEntryRequestModel);
            HttpResponseMessage response = null;
            using (var stringContent = new StringContent(s, System.Text.Encoding.UTF8, "application/json"))
            {

                if (IsHeaderRequired)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objHeaderModel.AccessToken);

                }
                response = await client.PostAsync(uri, stringContent);


                if (response.IsSuccessStatusCode)
                {
                    var SucessResponse = await response.Content.ReadAsStringAsync();
                    var statuscode = response.StatusCode;
                    StatusCodeClass.StatusCode = statuscode.ToString();
                    // SucessResponse = SucessResponse.Insert(1, "{" + "\"Status\"" + _col + "\"Success\"}");
                    objSendDiaryEntryResponseModel = JsonConvert.DeserializeObject<SendDiaryEntryResponseModel>(SucessResponse);
                    return objSendDiaryEntryResponseModel;
                }
                else
                {
                    var ErrorResponse = await response.Content.ReadAsStringAsync();
                    var statuscode = response.StatusCode;
                    StatusCodeClass.StatusCode = statuscode.ToString();
                    // ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"Fail\",");
                    objSendDiaryEntryResponseModel = JsonConvert.DeserializeObject<SendDiaryEntryResponseModel>(ErrorResponse);
                    return objSendDiaryEntryResponseModel;
                }
            }


        }

        public async Task<string> SendGivenAnswerAsync(string uri, Boolean IsHeaderRequired, HeaderModel objHeaderModel, List<AnswerValues> GivenAnswers)
        {
        
            SendGivenAnswerResponseModel objSendGivenAnswerResponseModel;
            string s = JsonConvert.SerializeObject(GivenAnswers);
            HttpResponseMessage response = null;
            using (var stringContent = new StringContent(s, System.Text.Encoding.UTF8, "application/json"))
            {

                if (IsHeaderRequired)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objHeaderModel.AccessToken);

                }
                response = await client.PostAsync(uri, stringContent);


                if (response.IsSuccessStatusCode)
                {
                    var SucessResponse = await response.Content.ReadAsStringAsync();
                    var statuscode = response.StatusCode;
                    StatusCodeClass.StatusCode = statuscode.ToString();
                    // SucessResponse = SucessResponse.Insert(1, "{" + "\"Status\"" + _col + "\"Success\"}");
                  string  res = JsonConvert.DeserializeObject<string>(SucessResponse);
                    return res;
                }
                else
                {
                    var ErrorResponse = await response.Content.ReadAsStringAsync();
                    var statuscode = response.StatusCode;
                    StatusCodeClass.StatusCode = statuscode.ToString();
                    // ErrorResponse = ErrorResponse.Insert(1, "\"Status\"" + _col + "\"Fail\",");
                    string res = JsonConvert.DeserializeObject<string>(ErrorResponse);
                    return res;
                }
            }


        }
        public async Task<string> PostAsyncData(string uri)
        {
            try
            {

                HttpClient httpClient = new HttpClient();
                HttpResponseMessage response = await httpClient.GetAsync(uri);
                return await response.Content.ReadAsStringAsync(); ;
            }
            catch (Exception ex)
            {

                throw;
                var message = ex.Message;
            }
        }

        public void FetchData(string url)
        {

            // Create an HTTP web request using the URL:
            webRequest = WebRequest.Create(new Uri(url));
            webRequest.ContentType = "application/json";
            webRequest.Method = "GET";
            webRequest.BeginGetRequestStream(new AsyncCallback(RequestStreamCallback), webRequest);
        }

        private void RequestStreamCallback(IAsyncResult asynchronousResult)
        {
            webRequest = (HttpWebRequest)asynchronousResult.AsyncState;

            using (var postStream = webRequest.EndGetRequestStream(asynchronousResult))
            {
                //send yoour data here
            }
            webRequest.BeginGetResponse(new AsyncCallback(ResponseCallback), webRequest);
        }


        void ResponseCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest myrequest = (HttpWebRequest)asynchronousResult.AsyncState;
            using (HttpWebResponse response = (HttpWebResponse)myrequest.EndGetResponse(asynchronousResult))
            {
                using (System.IO.Stream responseStream = response.GetResponseStream())
                {
                    using (var reader = new System.IO.StreamReader(responseStream))
                    {
                        var data = reader.ReadToEnd();
                    }
                }
            }
        }


        //public static string TOTPGenerator(string uniqueIdentity)
        //{
        //    string oneTimePassword = "";
        //    DateTime dateTime = DateTime.Now;
        //    string _strParsedReqNo = dateTime.Day.ToString();
        //    _strParsedReqNo = _strParsedReqNo + dateTime.Month.ToString();
        //    _strParsedReqNo = _strParsedReqNo + dateTime.Year.ToString();
        //    _strParsedReqNo = _strParsedReqNo + dateTime.Hour.ToString();
        //    _strParsedReqNo = _strParsedReqNo + dateTime.Minute.ToString();
        //    _strParsedReqNo = _strParsedReqNo + dateTime.Second.ToString();
        //    _strParsedReqNo = _strParsedReqNo + dateTime.Millisecond.ToString();
        //    _strParsedReqNo = _strParsedReqNo + uniqueIdentity;
        //    //Append above string with Policy number to get desired output.
        //   // Console.WriteLine("TOTP value: " + _strParsedReqNo);
        //    using (MD5 md5 = MD5.Create())
        //    {
        //        //Get hash code of entered request id in byte format.
        //        byte[] _reqByte = md5.ComputeHash(Encoding.UTF8.GetBytes(_strParsedReqNo));
        //        //convert byte array to integer.
        //        int _parsedReqNo = BitConverter.ToInt32(_reqByte, 0);
        //        string _strParsedReqId = Math.Abs(_parsedReqNo).ToString();
        //        //Check if length of hash code is less than 9.
        //        //If so, then prepend multiple zeros upto the length becomes atleast 9 characters.
        //        if (_strParsedReqId.Length < 9)
        //        {
        //            StringBuilder sb = new StringBuilder(_strParsedReqId);
        //            for (int k = 0; k < (9 - _strParsedReqId.Length); k++)
        //            {
        //                sb.Insert(0, '0');
        //            }
        //            _strParsedReqId = sb.ToString();
        //        }
        //        oneTimePassword = _strParsedReqId;
        //    }
        //    //Adding random letters to the OTP.
        //    StringBuilder builder = new StringBuilder();
        //    Random random = new Random();
        //    string randomString = "";
        //    for (int i = 0; i < 4; i++)
        //    {
        //        randomString += Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
        //    }
        //    //Deciding number of characters in OTP.
        //    Random ran = new Random();
        //    int randomNumber = ran.Next(2, 5);
        //    Random num = new Random();
        //    //Form alphanumeric OTP and rearrange it reandomly.
        //    string otpString = randomString.Substring(0, randomNumber);
        //    otpString += oneTimePassword.Substring(0, 7 - randomNumber);
        //    oneTimePassword = new string(otpString.ToCharArray().OrderBy(s => (num.Next(2) % 2) == 0).ToArray());
        //    return oneTimePassword;
        //}
    }
}
