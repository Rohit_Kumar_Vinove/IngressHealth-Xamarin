﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health_Api.ApiHandler
{
    public class Get_API_Url
    {
        // for Login
        public string LoginApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        // for register
        public string RegisterApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        public string PageTypeApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        //for survey 
        public string SurveyApi(string BaseUsrl)
        {

            return BaseUsrl;


        }

        // for Button frageBogen
        public string BtnFrageBogenApi(string BaseUsrl ,long SurveyId)
        {
            return string.Format("{0}{1}", BaseUsrl, SurveyId);
        }
        public string AnswerOptionsApi(string BaseUsrl, long PageId)
        {
            return string.Format("{0}{1}", BaseUsrl, PageId);
        }
        public string SaveGivenAnswerApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        public string GlossorApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        public string FAQApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        public string ResetpasswordApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        public string ChangeEmailApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        public string ChangepasswordApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        public string ShowPackage2BuyApi(string BaseUsrl)
        {
            return BaseUsrl;
        }
        public string GetAnswerBySurveryIdApi(string BaseUsrl, long SurveyId)
        {
            return string.Format("{0}{1}", BaseUsrl, SurveyId);
        }
        public string GetDiaryEntryApi(string BaseUrl)
        {
            return BaseUrl;
        }
        public string GetSymptomsListApi(string BaseUrl)
        {
            return BaseUrl;
        }
        public string GetGraphDataApi(string BaseUsrl, long SymptomId)
        {
            return string.Format("{0}{1}", BaseUsrl, SymptomId);
        }
        public string SendGivenAnswerApi(string BaseUrl)
        {
            return BaseUrl;
        }
        public string LoadGivenAnswerApi(string BaseUrl,int PageId)
        {
            return string.Format("{0}{1}", BaseUrl, PageId);
        }
        
    }
}
