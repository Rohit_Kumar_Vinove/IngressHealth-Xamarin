﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using AsNum.XFControls.Droid;
using PayPal.Forms.Abstractions;
using PayPal.Forms.Abstractions.Enum;
using Android.Content;
using PayPal.Forms;
using Syncfusion.SfChart.XForms.Droid;

namespace Ingress_Health.Droid
{
    //[Activity(Label = "Mobile App", Theme = "@style/splashscreen", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, LaunchMode = LaunchMode.SingleTop)]
    //public class MainActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity

   [Activity(Label = "Ingress_Health", Icon = "@drawable/icon", Theme = "@style/splashscreen", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.SensorPortrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.Window.RequestFeature(WindowFeatures.ActionBar);
            // Name of the MainActivity theme you had there before.
            // Or you can use global::Android.Resource.Style.ThemeHoloLight
            base.SetTheme(Resource.Style.MainTheme);
            base.OnCreate(bundle);
            AsNumAssemblyHelper.HoldAssembly();
            global::Xamarin.Forms.Forms.Init(this, bundle);
            new SfChartRenderer();
            PayPal.Forms.CrossPayPalManager.Init(
                new PayPalConfiguration(
                    PayPalEnvironment.NoNetwork,
                    "Aan8SJnGc__DQNN2gxtyKHM5HfDVGbn2kgDoWREkCSDktYuPyI7HP-y9vENV0QSbsft2oSp9iZ6r2c17"
                )
                {
                    AcceptCreditCards = false,
                    MerchantName = "Test Store",
                    MerchantPrivacyPolicyUri = "https://www.example.com/privacy",
                    MerchantUserAgreementUri = "https://www.example.com/legal",
                    // OPTIONAL - ShippingAddressOption (Both, None, PayPal, Provided)
                    //  ShippingAddressOption = ShippingAddressOption.Both,
                    // OPTIONAL - Language: Default languege for PayPal Plug-In
                      Language = "en",
                    // OPTIONAL - PhoneCountryCode: Default phone country code for PayPal Plug-In
                     PhoneCountryCode = "91",
                }
            );
            LoadApplication(new App());
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            PayPalManagerImplementation.Manager.OnActivityResult(requestCode, resultCode, data);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            PayPalManagerImplementation.Manager.Destroy();
        }
    }


}

