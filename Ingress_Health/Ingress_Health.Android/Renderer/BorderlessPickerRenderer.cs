﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System.ComponentModel;
using Android.Graphics.Drawables;
using Ingress_Health.Controls;
using Ingress_Health.Droid.Renderer;

[assembly: ExportRenderer(typeof(BorderLessPicker), typeof(BorderlessPickerRenderer))]

namespace Ingress_Health.Droid.Renderer
{
    public class BorderlessPickerRenderer : PickerRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
               // Control.Background = null;
            }
            SetControlStyle();
            // Control?.SetBackgroundColor(Android.Graphics.Color.Transparent);
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            SetControlStyle();
        }
        private void SetControlStyle()
        {
            if (Control != null)
            {
                Drawable imgDropDownArrow = Context.GetDrawable(Resource.Drawable.arrow);
                imgDropDownArrow.SetBounds(0, 0, 20, 0);
                Control.SetCompoundDrawablesRelativeWithIntrinsicBounds(null, null, imgDropDownArrow, null);
            }
        }
    }
}