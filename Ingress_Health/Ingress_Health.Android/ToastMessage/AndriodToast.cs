﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Ingress_Health.Interface;
using Plugin.CurrentActivity;
using Xamarin.Forms;
using Ingress_Health.Droid.ToastMessage;

[assembly: Dependency(typeof(AndriodToast))]
namespace Ingress_Health.Droid.ToastMessage
{
   public class AndriodToast :IToast
    {
        public void ShowToast(string message)
        {
            Activity activity = CrossCurrentActivity.Current.Activity;
            Toast.MakeText(Forms.Context, message, ToastLength.Long).Show();
        }
    }
}