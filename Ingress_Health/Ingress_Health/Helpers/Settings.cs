// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;

namespace Ingress_Health.Helpers
{
  /// <summary>
  /// This is the Settings static class that can be used in your Core solution or in any
  /// of your client applications. All settings are laid out the same exact way with getters
  /// and setters. 
  /// </summary>
  public static class Settings
  {
    private static ISettings AppSettings
    {
      get
      {
        return CrossSettings.Current;
      }
    }


        #region Setting First Page

        private const string firstPage = "firstPage_key";
        private static readonly bool firstPageDefault = false;


        public static bool FirstPage
        {
            get { return AppSettings.GetValueOrDefault("firstPage", firstPageDefault);
            }
            set
            { AppSettings.AddOrUpdateValue("firstPage", value); }
        }

        //public static string FirstPage
        //{
        //    get
        //    {
        //        return AppSettings.GetValueOrDefault<bool>(firstPage, firstPageDefault);
        //    }
        //    set
        //    {
        //        AppSettings.AddOrUpdateValue<string>(firstPage, value);
        //    }
        //}
        #endregion

        #region Setting URL

        private const string url = "url_key";
    private static readonly string urlDefault = string.Empty;

   


    public static string Url
    {
      get
      {
        return AppSettings.GetValueOrDefault<string>(url, urlDefault);
      }
      set
      {
        AppSettings.AddOrUpdateValue<string>(url, value);
      }
    }
        #endregion


        public static List<int> _surveyIdList;
        public static List<int> _surveyIdListDynamic;

        #region Setting Username
        private const string username = "username_key";
        private static readonly string usernameDefault = string.Empty;

        public static string Username
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(username, usernameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(username, value);
            }
        }
        #endregion
        #region Setting Password

        private const string password = "password_key";
        private static readonly string passwordDefault = string.Empty;

        public static string Password
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(password, passwordDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(password, value);
            }
        }
        #endregion

        #region Setting Access Token

        private const string access_token = "access_token_key";
        private static readonly string access_tokenDefault = string.Empty;

        public static string Access_token
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(access_token, access_tokenDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(access_token, value);
            }
        }
        #endregion

        #region Setting Access Token Expiration

        //private const string accessTokenExpiration = "access_token_key";
        //private static readonly string accessTokenExpirationDefault = DateTime;

        public static DateTime TokenExpiration
        {
            get
            {
                return AppSettings.GetValueOrDefault<DateTime>("accessTokenExpiration", DateTime.UtcNow);
            }
            set
            {
                AppSettings.AddOrUpdateValue<DateTime>("accessTokenExpiration", value);
            }
        }
        #endregion

        #region Setting token_type

        private const string token_type = "token_type_key";
        private static readonly string token_typeDefault = string.Empty;

        public static string Token_type
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(token_type, token_typeDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(token_type, value);
            }
        }
        #endregion

        #region IsLoggedIn

        private const string isLoggedIn = "isLoggedIn_key";
        private static readonly bool isLoggedInDefault = false;


        public static bool IsLoggedIn
        {
            get
            {
                return AppSettings.GetValueOrDefault("isLoggedIn", isLoggedInDefault);
            }
            set
            { AppSettings.AddOrUpdateValue("isLoggedIn", value); }
        }


        #endregion

        #region SurveyId

        private const string surveyid = "surveyid_key";
        private static readonly int surveyidDefault = 0;


        public static int SurveyId
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(surveyid, surveyidDefault);
            }
            set
            { AppSettings.AddOrUpdateValue(surveyid, value); }
        }


        #endregion
    }
}