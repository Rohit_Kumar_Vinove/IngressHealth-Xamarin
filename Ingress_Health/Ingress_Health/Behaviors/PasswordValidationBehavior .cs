﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Ingress_Health
{
  public  class PasswordValidationBehavior : Behavior<Entry>
    {
        const string passwordRegex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$";
      //  const string passwordRegex = @" ^ (?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$";

        static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(PasswordValidationBehavior), false);

        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

        public bool IsValid
        {
            get { return (bool)base.GetValue(IsValidProperty); }
            private set { base.SetValue(IsValidPropertyKey, value); }
        }

        protected override void OnAttachedTo(Entry bindable)

        {

            bindable.TextChanged += HandleTextChanged;

            base.OnAttachedTo(bindable);

        }


        void HandleTextChanged(object sender, TextChangedEventArgs e)

        {

          //  bool IsValid = false;

            IsValid = (Regex.IsMatch(e.NewTextValue, passwordRegex));

            ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Red;

        }


        protected override void OnDetachingFrom(Entry bindable)

        {

            bindable.TextChanged -= HandleTextChanged;

            base.OnDetachingFrom(bindable);

        }

    }
}

