﻿using Ingress_Health.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Ingress_Health.Controls
{
    public class Expander : ContentView
    {
        public event EventHandler Clicked;

        protected Grid ContentGrid;
        protected Button btnControl;
        public Grid ContentContainer = new Grid();
      //  public Label HeaderContainer;

        private Boolean _IsExpanded;
        public Boolean IsExpanded
        {
            get { return _IsExpanded; }
            set
            {
                int i=0;
                _IsExpanded = value;
                foreach (var item in FAQPage.GetQuestForFaq)
                {
                    if (_IsExpanded)
                    {
                        ContentContainer.IsVisible = true;

                        btnControl.Text = item.Question[i].ToString();
                    }
                    else
                    {
                        ContentContainer.IsVisible = false;
                        btnControl.Text = item.Question[i].ToString();
                    }
                }
                i++;  
            }
        }

        public Expander()
        {
            try { 
            ContentGrid = new Grid
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

           // ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(42) });
           // ContentGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0, GridUnitType.Star) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            ContentGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            btnControl = new Button
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor=Color.FromHex("#0070BA")
            };
            btnControl.HeightRequest = 42;
          //  btnControl.WidthRequest = 42;
            IsExpanded = false;
            btnControl.Clicked += Btn_Clicked;
            ContentGrid.Children.Add(btnControl);

            ContentContainer.VerticalOptions = LayoutOptions.FillAndExpand;
            ContentContainer.HorizontalOptions = LayoutOptions.FillAndExpand;
            Grid.SetRow(ContentContainer, 1);
         //   Grid.SetColumnSpan(ContentContainer, 2);
            ContentGrid.Children.Add(ContentContainer);

            //HeaderContainer = new Label
            //{
            //    TextColor = Color.White,
            //    VerticalOptions = LayoutOptions.Center,
            //    HorizontalOptions = LayoutOptions.FillAndExpand,
            //};
            //Grid.SetColumn(HeaderContainer, 1);
            //ContentGrid.Children.Add(HeaderContainer);

            base.Content = ContentGrid;

            this.BackgroundColor = Color.Transparent;
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }

        private async void Btn_Clicked(object sender, EventArgs e)
        {
            IsExpanded = !IsExpanded;
        }

        //public static BindableProperty HeaderProperty = BindableProperty.Create(
        //    propertyName: "Header",
        //    returnType: typeof(String),
        //    declaringType: typeof(Expander),
        //    defaultValue: null,
        //    defaultBindingMode: BindingMode.TwoWay,
        //    propertyChanged: HeaderValueChanged);

        //private static void HeaderValueChanged(BindableObject bindable, object oldValue, object newValue)
        //{
        //    ((Expander)bindable).HeaderContainer.Text = (String)newValue;
        //}

      //  public event EventHandler HeaderChanged;
        //private void RaiseHeaderChanged()
        //{
        //    if (HeaderChanged != null)
        //        HeaderChanged(this, EventArgs.Empty);
        //}

        //public String Header
        //{
        //    get { return (String)GetValue(HeaderProperty); }
        //    set
        //    {
        //        SetValue(HeaderProperty, value);
        //        OnPropertyChanged();
        //        RaiseHeaderChanged();
        //    }
        //}

        public View content
        {
            get { return ContentContainer.Children[0]; }
            set
            {
                ContentContainer.Children.Clear();
                ContentContainer.Children.Add((View)value);
                OnPropertyChanged();
            }
        }

    }
}
