﻿using Ingress_Health.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace Ingress_Health.ViewModels
{
   public class RegisterViewModel
    {
        ApiServices _apiServicec = new ApiServices();

        public int Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string Message { get; set; }
        //public ICommand RegisterCommand
            
        //{
        //    get
        //    {
        //        return new Command(async() =>
        //        {
        //       var isSucess = await _apiServicec.RegisterAsync(Email, Password, ConfirmPassword);

        //            if(isSucess)
        //            {
        //                Message = "Registerd Sucessfully!";
        //            }
        //            else
        //            {
        //                Message = "Retry Later!";
        //            }
        //        });
        //    }
        //}
    }
}
