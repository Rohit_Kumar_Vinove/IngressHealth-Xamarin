﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ingress_Health.Pages;
using Xamarin.Forms;
using Ingress_Health.Interface;
using Ingress_Health_Api.RequestModel;
using Ingress_Health.Helpers;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.ResponseModel;
using Ingress_Health_Api.Model;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;

namespace Ingress_Health
{
    public partial class App : Application
    {
        #region   varibale Declaration 
        

        private string _baseUrl;
        private RestApi _apiServices;
        private SurveyResponseModel _objSurveyResPonseModel;  
        private SurveyRequestModel _objSurveyRequestModel; 
        private string Url = "https://msapp.into-medpharm.com/";
        string[] items = new string[3];      
        public static bool IsLoggedIn { get; set; }
        #endregion


        #region Constructor
        public App()
        {
            InitializeComponent();
            _baseUrl = Url + Domain.GetSurveyApiConstant;
            _objSurveyResPonseModel= new SurveyResponseModel();
            _objSurveyRequestModel= new SurveyRequestModel();
            _apiServices = new RestApi();
            LoadPageData();
            Task.Delay(5000);
            if (DateTime.UtcNow.AddHours(1) > Settings.TokenExpiration)
            {
                Settings.Access_token = string.Empty;
            }
            Settings.Url = Domain.Url;
          //  IsLoggedIn = false;
            SetMainpage();
          
          //  MainPage= new RadioButtonPage();

        }
        #endregion

        #region Methods&Events
        private void SetMainpage()
        {
            if (Settings.FirstPage == false)
            {
               
                MainPage = new NavigationPage(new MainPage());
            }
            else if (!string.IsNullOrEmpty(Settings.Access_token))
            {
                MainPage = new NavigationPage(new MenuPage());

            }
            else
            {
                MainPage = new NavigationPage(new LoginPage());
            }
            
        }
        /// <summary>
        /// Method for loading the page data
        /// </summary>
        private  void LoadPageData()
        {
            try
            {
                HeaderModel objHeaderModel = new HeaderModel();
                objHeaderModel.AccessToken = Settings.Access_token;            
                _objSurveyResPonseModel.GetSurveyList = Task.Run(async () => await _apiServices.GetAsyncData_GetApiList(new Get_API_Url().SurveyApi(_baseUrl), true, objHeaderModel, _objSurveyResPonseModel.GetSurveyList)).Result;              
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public  void imgHome_Click(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PushAsync(new MenuPage(),true);
               // MainPage = new NavigationPage( new MenuPage());           

            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private  void imgList_Click(object sender, EventArgs e)
        {
            LoadPageData();
            try
            {
                App.Current.MainPage.Navigation.PushAsync(new PatientLikeMePage(_objSurveyResPonseModel.GetSurveyList[2]),true);
               // MainPage = new PatientLikeMePage();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private  void imgProfile_Click(object sender, EventArgs e)
        {
            LoadPageData();
       try
            {
                App.Current.MainPage.Navigation.PushAsync(new MY_MS_DiaryPage(_objSurveyResPonseModel.GetSurveyList[3]),true);
               // MainPage = new MenuPage();
            }
           catch (Exception ex)
            {
                var message = ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private  void imgSetting_Click(object sender, EventArgs e)
        {  
            try
            {
                App.Current.MainPage.Navigation.PushAsync(new SettingPage(),true);
              //  MainPage = new SettingPage();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }
        /// <summary>
        /// when app is start
        /// </summary>
        protected override void OnStart()
        {
            // Handle when your app starts
        }
        /// <summary>
        /// when app is in backGround
        /// </summary>
        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }
        /// <summary>
        /// when app is resumed from back ground
        /// </summary>
        protected override void OnResume()
        {
            // Handle when your app resumes
        }
        #endregion
        //private void OnBackButtonPressed(object sender, EventArgs e)
        //{
        //    try
        //    {
        //         App.Current.MainPage.Navigation.PopAsync();
        //    }
        //   catch(Exception ex)
        //    { }

        //}
        //public void ShowMainPage()
        //{
        //    MainPage = new MainPage();
        //}

        //public void Logout()
        //{
        //    Properties["IsLoggedIn"] = false; // only gets set to 'true' on the LoginPage
        // //   MainPage = new LoginModalPage(this);
        //}
    }
}
