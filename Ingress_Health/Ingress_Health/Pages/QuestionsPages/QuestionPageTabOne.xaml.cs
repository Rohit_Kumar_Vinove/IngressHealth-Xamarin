﻿using AsNum.XFControls;
using Ingress_Health.Controls;
using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health.Pages;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Controls;

namespace Ingress_Health.Pages.QuestionsPages
{

    public class RadioItem
    {
        public string Name { get; set; }
        public int ID { get; set; }

    }
    public class Tempdata
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int? PageId { get; set; }
        public int AnswerType { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuestionPageTabOne : ContentPage
    {       
        #region   varibale Declaration

        private string _baseUrl;
        private string _baseUrlSendAns;
        private string _baseUrlAns;
        private string _baseUrlLoadAns;
        private string _UrlAns;
        private int AnsId4Text;
        private int index=0;
        private List<LoadGivenAnswerRequestModel> _objLoadGivenAnsList;
        private bool _pagedata = false;
        private static int _surveyId;
        private static int PageIDE;
        private int CurrentId;
        public static int _resultpageId;
        private string _baseUrlNextQuestion;
        private RestApi _apiServices;
        private  HeaderModel objHeaderModel;
        private ResultPageResponseModel _objResultPageResponseModel;
        private PageTypeResponseModel _objPageTypeResPonseModel;
        private SurveyRequestModel _objSurveyRequestModel;
        private LoadAnswerOptionsResponseModel _objLoadAnswerOptionsResponseModel;
        private LoadAnswerOptionsRequestModel _objLoadAnswerOptionsRequestModel;
        private SendGivenAnswerRequestModel _objSendGivenAnswerRequestModel;
        private SendGivenAnswerResponseModel _objSendGivenAnswerResponseModel;
        private LoadGivenAnswerResponseModel _objLoadGivenAnswerResponseModel;
        private LoadGivenAnswerRequestModel _objLoadGivenAnswerRequestModel;
        AnswerValues _objAnswerValues;
       // RadioGroup rdbtn;
        AsNum.XFControls.CheckBox chkbx;
       // Radio radio;
       // CustomRadioButton cstnRdbtn;
        Controls.BindableRadioGroup bdrgp;
        Entry entry;
        Editor editor;
       // RadioButtonGroup rdbgrp;
        List<AsNum.XFControls.CheckBox> objCheckBoxList;
       // List<CustomRadioButton> objRadioBoxList;
        List<int> allAnswerIds = new List<int>();
        List<int> allAnsType = new List<int>();
        List<string> Desc = new List<string>();
        #endregion
        #region Constructor
        public QuestionPageTabOne(SurveyRequestModel objSurveyRequestModel, int turn)
        {
            InitializeComponent();
            //this.BindingContext = this;
            _objLoadAnswerOptionsResponseModel = new LoadAnswerOptionsResponseModel();
            _objLoadAnswerOptionsRequestModel = new LoadAnswerOptionsRequestModel();
            _objSendGivenAnswerResponseModel = new SendGivenAnswerResponseModel();
            _objSendGivenAnswerRequestModel = new SendGivenAnswerRequestModel();
            _objLoadGivenAnswerRequestModel = new LoadGivenAnswerRequestModel();
            _objAnswerValues = new AnswerValues();    
            _objResultPageResponseModel = new ResultPageResponseModel();
            _objLoadGivenAnsList = new List<LoadGivenAnswerRequestModel>();
           // _objResultPageResponseModel.Id = _resultpageId;
             objHeaderModel= new HeaderModel();
            _baseUrlLoadAns = Settings.Url + Domain.LoadGivenAnswerApiConstent;
            _baseUrlSendAns = Settings.Url + Domain.SendGivenAnswerApiConstent;
            _baseUrl = Settings.Url + Domain.GetbtnFrageBogenApiConstant;
            _baseUrlAns = Settings.Url + Domain.GetAnswerOptionsApiConstant;
            _UrlAns = Settings.Url + Domain.GetResultPageApiConstent;
            _baseUrlNextQuestion = Settings.Url + Domain.GetNextPageQuestionApiConstant;
            _apiServices = new RestApi();
             
            _objLoadGivenAnswerResponseModel = new LoadGivenAnswerResponseModel();
            if (objSurveyRequestModel != null)
            {
                _objSurveyRequestModel = objSurveyRequestModel;
                _surveyId = _objSurveyRequestModel.Id;
                //_surveyIdList.Add(_objSurveyRequestModel.Id);
                //_surveyIdListDynamic.Add(_objSurveyRequestModel.Id);
               // PageIDE = _objSurveyRequestModel.Id;
            }           
            NavigationPage.SetHasNavigationBar(this, false);
            if (turn == 0)
            {
                CurrentId = _surveyId;
                LoadAnswerData(_baseUrl);
            }
            else
            {
                if (Settings._surveyIdListDynamic != null && Settings._surveyIdListDynamic.Count > 0)
                {
                    CurrentId = Settings._surveyIdListDynamic[Settings._surveyIdListDynamic.Count - 1];
                }
                    //CurrentId = PageIDE;
                    LoadAnswerData(_baseUrlNextQuestion);
               // PageIDE = _surveyId;
            }
           
        }
        #endregion
        #region Events & Methods
        protected override void OnAppearing()
        {

            // Set up activity indicator
            // Navigation.PushPopupAsync(new LoadingHorizonPage());
            LoadDataToxaml();
            // Navigation.PopAllPopupAsync();
            // Remove activity indicator and set up real views   

            GC.Collect();
        }
        private async Task LoadDataToxaml()
        {
            Questiontxt.Text = _objPageTypeResPonseModel.Description;
            headertxt.Text = _objPageTypeResPonseModel.Header;
           GC.Collect();
        }
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                if(Settings._surveyIdListDynamic != null && Settings._surveyIdListDynamic.Count > 0)
                {
                    Settings._surveyIdListDynamic.RemoveAt(Settings._surveyIdListDynamic.Count - 1);
                }
                if (Settings._surveyIdList != null && Settings._surveyIdList.Count>0)
                {
                    Settings._surveyIdList.RemoveAt(Settings._surveyIdList.Count - 1);
                }
                    App.Current.MainPage.Navigation.PopAsync();
                //LoadAnswerData(_baseUrlNextQuestion);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private async void LoadAnswerData(string Surveyurl)
        {
           
            try
            {                
                objHeaderModel.AccessToken = Settings.Access_token;
                _objPageTypeResPonseModel = Task.Run(async () => await _apiServices.GetAsyncData_GetApi(new Get_API_Url().BtnFrageBogenApi(Surveyurl, CurrentId), true, objHeaderModel, _objPageTypeResPonseModel)).Result;
              
               // PageIDE = _objPageTypeResPonseModel.Id;
                if (Settings._surveyIdList != null && Settings._surveyIdList.Count>1)
                {
                    Settings._surveyIdList.Add(_objPageTypeResPonseModel.Id);
                    PageIDE = Settings._surveyIdList[Settings._surveyIdList.Count-1];
                }
                else
                {
                    Settings._surveyIdList.Add(_objPageTypeResPonseModel.Id);
                    PageIDE = Settings._surveyIdList[Settings._surveyIdList.Count-1];                 
                }             
                if (_objPageTypeResPonseModel.Id != 0)
                {
             
                   var pageId = _objPageTypeResPonseModel.Id;
                    _objLoadAnswerOptionsResponseModel.GetAnswersList = Task.Run(async () => await _apiServices.GetAsyncData_GetApiList(new Get_API_Url().AnswerOptionsApi(_baseUrlAns, pageId), true, objHeaderModel, _objLoadAnswerOptionsResponseModel.GetAnswersList)).Result;
                    var Ans = _objLoadAnswerOptionsResponseModel.GetAnswersList;
                    if (_objPageTypeResPonseModel.PageType == Convert.ToInt32(AnswerTypes.Radiobutton))
                    {                        
                        IsAnsSelected();
                        if (_objLoadGivenAnsList.Count > 0)
                        {
                            int i = 0;
                            
                            foreach (var itemsIs in _objLoadAnswerOptionsResponseModel.GetAnswersList)
                            {                            
                                for (int j = 0; j < _objLoadGivenAnsList.Count; j++)

                                {
                                    if (_objLoadGivenAnsList[j].AnswerId == _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id)
                                    {
                                        index =i;
                                    }
                                }
                                i++;
                            }                           
                            foreach (var item in _objLoadAnswerOptionsResponseModel.GetAnswersList)
                            {
                                Desc.Add(item.Description);
                            }                          
                            bdrgp = new Controls.BindableRadioGroup()
                            {
                                ItemsSource = Desc,
                                SelectedIndex = index,                               
                                    };
                            bdrgp.CheckedChanged += Bdrgp_CheckedChanged;                         
                        }
                        else
                        {                           
                            foreach(var item in _objLoadAnswerOptionsResponseModel.GetAnswersList)
                            {
                                Desc.Add(item.Description);
                            }
                            bdrgp = new Controls.BindableRadioGroup();                           
                            bdrgp.ItemsSource = Desc;                        
                            bdrgp.CheckedChanged += Bdrgp_CheckedChanged;                             
                        }

                        displayAns.Children.Add(bdrgp);
                    }
                    else if (_objPageTypeResPonseModel.PageType == Convert.ToInt32(AnswerTypes.Radiobutton_and_Text))
                    {
                        IsAnsSelected();
                        entry = new Entry
                        {
                            Placeholder = "Enter Your Answer here!",
                            VerticalOptions = LayoutOptions.End,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                        };
                        displayAns.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                        displayAns.RowDefinitions.Add(new RowDefinition { Height = new GridLength(60, GridUnitType.Absolute) });
                        if (_objLoadGivenAnsList.Count > 0)
                        {
                            int i = 0;

                            foreach (var Item in _objLoadAnswerOptionsResponseModel.GetAnswersList)
                            {
                               // displayAns.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                                for (int j = 0; j < _objLoadGivenAnsList.Count; j++)
                                {
                                    if (_objLoadGivenAnsList[j].AnswerId == _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id)
                                    {
                                        index = i;
                                        if(_objLoadGivenAnsList[j].AnswerId == _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id && _objLoadAnswerOptionsResponseModel.GetAnswersList[i].AnswerType==2)
                                        {
                                            entry.Text = _objLoadGivenAnsList[j].Value;
                                        }
                                    }
                                }
                                i++;
                            }
                            
                            foreach (var item1 in _objLoadAnswerOptionsResponseModel.GetAnswersList)
                            {
                                Desc.Add(item1.Description);
                            }
                            bdrgp = new Controls.BindableRadioGroup()
                            {
                                ItemsSource = Desc,
                                SelectedIndex = index,
                            };
                            bdrgp.CheckedChanged += Bdrgp_CheckedChanged;
                           
                          
                        }
                        else
                        {
                            foreach (var item in _objLoadAnswerOptionsResponseModel.GetAnswersList)
                            {
                                Desc.Add(item.Description);
                            }
                            bdrgp = new Controls.BindableRadioGroup();
                            bdrgp.ItemsSource = Desc;
                            bdrgp.CheckedChanged += Bdrgp_CheckedChanged;
                                                     
                        }
                        displayAns.Children.Add(bdrgp, 0, 0);
                        displayAns.Children.Add(entry, 0, 1);
                        
                    }
                    else if (_objPageTypeResPonseModel.PageType == Convert.ToInt32(AnswerTypes.Checkbox))                    
                    {
                        IsAnsSelected();                       
                        objCheckBoxList = new List<AsNum.XFControls.CheckBox>();
                        if (_objLoadGivenAnsList.Count > 0)
                        {
                            int i = 0;
                            //for (i = 0; i < _objLoadAnswerOptionsResponseModel.GetAnswersList.Count; i++)
                            foreach (var itemsIs in _objLoadAnswerOptionsResponseModel.GetAnswersList)
                            {
                                chkbx = new AsNum.XFControls.CheckBox();
                                displayAns.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                                for (int j = 0; j < _objLoadGivenAnsList.Count; j++)

                                {
                                    if (_objLoadGivenAnsList[j].AnswerId == _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id)
                                    {
                                        chkbx.Checked = true;
                                    }
                                }
                                chkbx.Text = _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Description;
                                chkbx.ShowLabel = true;
                                chkbx.OnImg = "ck1.png";
                                chkbx.OffImg = "ck2.png";

                                objCheckBoxList.Add(chkbx);
                                allAnswerIds.Add(_objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id);
                                displayAns.Children.Add(chkbx, 0, i);
                               
                                i++;
                            }
                        }

                        else
                        {
                            int i = 0;
                            for (i = 0; i < _objLoadAnswerOptionsResponseModel.GetAnswersList.Count; i++)
                            {
                                displayAns.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                                chkbx = new AsNum.XFControls.CheckBox();
                                chkbx.Checked = false;
                                chkbx.Text = _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Description;
                                chkbx.ShowLabel = true;
                                chkbx.OnImg = "ck1.png";
                                chkbx.OffImg = "ck2.png";

                                objCheckBoxList.Add(chkbx);
                                allAnswerIds.Add(_objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id);
                                displayAns.Children.Add(chkbx, 0, i);                  
                            }
                        }
                    }
                    else if (_objPageTypeResPonseModel.PageType == Convert.ToInt32(AnswerTypes.Checkbox_and_Text))
                    {
                        IsAnsSelected();

                        entry = new Entry
                        {
                            Placeholder = "Enter Your Answer here!",
                            VerticalOptions = LayoutOptions.End,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                        };
                        objCheckBoxList = new List<AsNum.XFControls.CheckBox>();
                        if (_objLoadGivenAnsList.Count > 0)
                        {
                            int i = 0;
                            //for (i = 0; i < _objLoadAnswerOptionsResponseModel.GetAnswersList.Count; i++)
                            foreach (var itemsIs in _objLoadAnswerOptionsResponseModel.GetAnswersList)
                            {
                                chkbx = new AsNum.XFControls.CheckBox();
                                displayAns.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });                                                                   
                                    for (int j = 0; j < _objLoadGivenAnsList.Count; j++)

                                    {                                       
                                        if (_objLoadGivenAnsList[j].AnswerId == _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id)
                                        {
                                            chkbx.Checked = true;  
                                        if(_objLoadGivenAnsList[j].AnswerId == _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id && _objLoadAnswerOptionsResponseModel.GetAnswersList[i].AnswerType==4)
                                        {
                                            entry.Text = _objLoadGivenAnsList[j].Value;
                                        }                                                                                            
                                        }                                       
                                    }                              
                                chkbx.Text = _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Description;
                                chkbx.ShowLabel = true;
                                chkbx.OnImg = "ck1.png";
                                chkbx.OffImg = "ck2.png";

                                objCheckBoxList.Add(chkbx);
                                allAnswerIds.Add(_objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id);
                                allAnsType.Add(_objLoadAnswerOptionsResponseModel.GetAnswersList[i].AnswerType);
                                displayAns.Children.Add(chkbx, 0, i);

                                if (i == _objLoadAnswerOptionsResponseModel.GetAnswersList.Count - 1)
                                {
                                    displayAns.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                                    displayAns.Children.Add(entry, 0, i + 1);
                                }
                                i++;
                            }
                        }

                        else
                           {
                            int i = 0;
                            for (i = 0; i < _objLoadAnswerOptionsResponseModel.GetAnswersList.Count; i++)
                            {
                                displayAns.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                                chkbx = new AsNum.XFControls.CheckBox();
                                chkbx.Checked = false;
                                chkbx.Text = _objLoadAnswerOptionsResponseModel.GetAnswersList[i].Description;
                                chkbx.ShowLabel = true;
                                chkbx.OnImg = "ck1.png";
                                chkbx.OffImg = "ck2.png";

                                objCheckBoxList.Add(chkbx);
                                allAnswerIds.Add(_objLoadAnswerOptionsResponseModel.GetAnswersList[i].Id);
                                allAnsType.Add(_objLoadAnswerOptionsResponseModel.GetAnswersList[i].AnswerType);
                                displayAns.Children.Add(chkbx, 0, i);

                                if (i == _objLoadAnswerOptionsResponseModel.GetAnswersList.Count - 1)
                                {
                                    displayAns.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                                    displayAns.Children.Add(entry, 0, i + 1);
                                }
                            }                               
                               }
                            
                        


                    }
                    else if (_objPageTypeResPonseModel.PageType == Convert.ToInt32(AnswerTypes.Text))
                    {
                        if(IsAnsSelected().Count>0)
                        {
                            editor = new Editor
                            {
                                TextColor = Color.Black,
                                VerticalOptions = LayoutOptions.Center,
                                IsVisible = true,
                                IsEnabled = true,
                                BackgroundColor = Color.Silver,
                                Text = _objLoadGivenAnsList.FirstOrDefault().Value,
                                // Text="Please Type Here",              
                                HorizontalOptions = LayoutOptions.FillAndExpand
                            };
                        }
                        else
                        {
                            editor = new Editor
                            {
                                TextColor = Color.Black,
                                VerticalOptions = LayoutOptions.Center,
                                IsVisible = true,
                                IsEnabled = true,
                                BackgroundColor = Color.Silver,                              
                                // Text="Please Type Here",              
                                HorizontalOptions = LayoutOptions.FillAndExpand
                            };
                        }

                        AnsId4Text = _objLoadAnswerOptionsResponseModel.GetAnswersList[0].Id;
                        displayAns.Children.Add(editor);
                    }
                    else if (_objPageTypeResPonseModel.PageType == Convert.ToInt32(AnswerTypes.Silder))
                    {
                        Slider slider = new Slider
                        {
                            Maximum = 100,
                            Minimum=1,
                            Value=70.00

                        };
                        displayAns.Children.Add(slider);
                    }
                    else if (_objPageTypeResPonseModel.PageType == Convert.ToInt32(AnswerTypes.Radiobutton_and_Date))
                    {

                    }
                    else if (_objPageTypeResPonseModel.PageType == Convert.ToInt32(AnswerTypes.Checkbox_and_Date))
                    {

                    }
                    else if (_objPageTypeResPonseModel.PageType == Convert.ToInt32(AnswerTypes.Date))
                    {

                    }
                     
                    else
                    {
                        DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please try Again!");
                        
                    }                  
                      //  _surveyId = _objPageTypeResPonseModel.Id;                                                                        
                }
                else
                {                   
                    DependencyService.Get<IToast>().ShowToast("This Is the last Question For Answer Thanks For Your Time!");                   
                    imgNext.IsVisible = false;                 
                    Button button = new Button
                    {
                        TextColor = Color.Black,
                        VerticalOptions = LayoutOptions.Center,
                        IsVisible = true,
                        IsEnabled = true,
                        BackgroundColor = Color.Silver,
                        Text = "Go to Answer Page",
                        HorizontalOptions = LayoutOptions.FillAndExpand
                    };
                    displayAns.Children.Add(button);
                    button.Clicked += Button_Clicked;
                }                
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }

       

        private void Bdrgp_CheckedChanged(object sender, int e)
        {
            _objLoadAnswerOptionsRequestModel = new LoadAnswerOptionsRequestModel();
            _objLoadAnswerOptionsRequestModel = _objLoadAnswerOptionsResponseModel.GetAnswersList[e];
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                objHeaderModel.AccessToken = Settings.Access_token;
                _objResultPageResponseModel = await _apiServices.GetAsyncData_GetApi(new Get_API_Url().GetAnswerBySurveryIdApi(_UrlAns, _resultpageId), true, objHeaderModel, _objResultPageResponseModel);

                if (_objResultPageResponseModel.Id > 0)
                {
                    switch (_objResultPageResponseModel.PageType)
                    {
                        case 6:
                            await Navigation.PushAsync(new OutPutPageOne(_objResultPageResponseModel, 1), true);
                            await Navigation.PopAllPopupAsync();
                            return;
                        case 7:
                            await Navigation.PushAsync(new OutputPageTwo(_objResultPageResponseModel, 1), true);
                            await Navigation.PopAllPopupAsync();
                            return;
                        case 8:
                            await Navigation.PushAsync(new OutputPageThree(_objResultPageResponseModel, 1), true);
                            await Navigation.PopAllPopupAsync();
                            return;
                        default:
                            return;

                    }

                }
                else
                {
                    DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please try Again!");
                    await Navigation.PopAllPopupAsync();
                }
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
                await Navigation.PopAllPopupAsync();
            }
            //switch (_resultpageId)
            //{
            //    case 6:
            //        await Navigation.PushAsync(new OutPutPageOne(_resultpageId, 1), true);
            //        break;
            //    case 7:
            //        await Navigation.PushAsync(new OutputPageTwo(_resultpageId, 1), true);
            //        break;
            //    case 8:
            //        await Navigation.PushAsync(new OutputPageThree(_resultpageId, 1), true);
            //        break;
            //}
            // await Navigation.PushAsync(new OutPutPageThree(_resultpageId), true);
        }
        private async void OnnextbtnPressed(object sender, EventArgs e)
        {
            List<AnswerValues> objAnswerValues = new List<AnswerValues>();
            _objSendGivenAnswerRequestModel.GivenAnswers = objAnswerValues;
            try
            {
                int i = 0;

                switch(_objPageTypeResPonseModel.PageType)
                {
                    case 1:
                        //foreach (Radio item in objRadioBoxList)
                        //{
                        //    //  AnswerValues _obj = new AnswerValues();

                        //    if (item.IsSelected)
                        //    {
                        //        _objAnswerValues.Value = item.Text;
                        //        _objAnswerValues.AnswerId = allAnswerIds[i];
                        //        _objAnswerValues.PageId = PageIDE;
                        //        objAnswerValues.Add(_objAnswerValues);
                        //    }
                        //    i++;
                        //}

                        //var itemSelected = rdbtn.SelectedItem as LoadAnswerOptionsRequestModel;
                        if (_objLoadAnswerOptionsRequestModel.Id != 0)
                        {
                            _objAnswerValues.Value = _objLoadAnswerOptionsRequestModel.Description;
                            _objAnswerValues.AnswerId = _objLoadAnswerOptionsRequestModel.Id;
                            _objAnswerValues.PageId = PageIDE;
                            objAnswerValues.Add(_objAnswerValues);
                            _pagedata = true;
                        }
                        else
                        {

                            DependencyService.Get<IToast>().ShowToast("please Select any suitable option to continue!");
                            _pagedata = false;
                        }
                        break;
                    case 2:
                        var radiotext = entry.Text;
                        if (_objLoadAnswerOptionsRequestModel.Id != 0)
                        {
                            if(_objLoadAnswerOptionsRequestModel.AnswerType== 2)
                            {
                                _objAnswerValues.Value = radiotext;
                            }
                            else
                            {
                                _objAnswerValues.Value = _objLoadAnswerOptionsRequestModel.Description;
                            }
                            _objAnswerValues.AnswerId = _objLoadAnswerOptionsRequestModel.Id;
                            _objAnswerValues.PageId = PageIDE;
                            objAnswerValues.Add(_objAnswerValues);
                            _pagedata = true;
                        }
                        else
                        {

                            DependencyService.Get<IToast>().ShowToast("please Select any suitable option to continue!");
                            _pagedata = false;

                        }
                        //var Selected = rdbtn.SelectedItem as LoadAnswerOptionsRequestModel;
                        //var Radiobtntext = entry.Text;
                        //if (Selected != null && Selected.Description.Trim()!= "Andere:")
                        //{
                        //    {
                        //        _objAnswerValues.Value = Selected.Description;
                        //        _objAnswerValues.AnswerId = Selected.Id;
                        //        _objAnswerValues.PageId = PageIDE;
                        //        objAnswerValues.Add(_objAnswerValues);
                        //        _pagedata = true;
                        //    }
                        //}
                        //else if (Selected != null && Selected.Description.Trim() == "Andere:")
                        //{
                        //    _objAnswerValues.Value = Radiobtntext;
                        //    _objAnswerValues.AnswerId = Selected.Id;
                        //    _objAnswerValues.PageId = PageIDE;
                        //    objAnswerValues.Add(_objAnswerValues);
                        //    _pagedata = true;
                        //}
                        //else
                        //{
                        //    DependencyService.Get<IToast>().ShowToast("please Select any suitable option to continue!");
                        //    _pagedata = false;
                        //}

                        break;
                    case 3:
                        foreach (AsNum.XFControls.CheckBox Sitem in objCheckBoxList)
                        {
                          //  AnswerValues _obj = new AnswerValues();
                            if (Sitem.Checked)
                            {                               
                                _objAnswerValues.Value = Sitem.Text;
                                _objAnswerValues.AnswerId = allAnswerIds[i];
                                _objAnswerValues.PageId = PageIDE;
                                objAnswerValues.Add(_objAnswerValues);                           
                            }                         
                            i++;
                        }
                        _pagedata = true;
                        break;
                    case 4:
                        // AnswerValues obj = new AnswerValues();
                        var chekboxtext = entry.Text;
                        foreach (AsNum.XFControls.CheckBox Citem in objCheckBoxList)
                        {                       
                        if(Citem.Checked)
                            {
                                _objAnswerValues = new AnswerValues();
                                if (allAnsType[i] == 4)
                                {
                                    _objAnswerValues.Value = chekboxtext;
                                }
                                else
                                {
                                    _objAnswerValues.Value = Citem.Text;
                                }
                               _objAnswerValues.AnswerId = allAnswerIds[i];
                                _objAnswerValues.PageId = PageIDE;
                                objAnswerValues.Add(_objAnswerValues);
                            }
                           
                            i++;                           
                        }                      
                        _pagedata = true;
                        break;
                    case 5:
                        var txtAns = editor.Text;
                        if(!string.IsNullOrEmpty(txtAns))
                        {
                            _objAnswerValues.Value = txtAns;
                            _objAnswerValues.PageId = PageIDE;
                             _objAnswerValues.AnswerId = AnsId4Text;
                            objAnswerValues.Add(_objAnswerValues);
                            _pagedata = true;
                        }
                        else
                        {
                            DependencyService.Get<IToast>().ShowToast("please enter suitable text to continue!");
                            _pagedata = false;
                        }
                        break;
                    case 6:
                       
                        break;
                    case 7:
                       
                        break;
                    case 8:
                      
                        break;
                    case 9:
                       
                        break;
                    default:
                       
                        break;
                }                                 
                   await Navigation.PushPopupAsync(new LoadingPopPage());             
                if (_pagedata)
                {
                    objHeaderModel.AccessToken = Settings.Access_token;
                    string rz = Task.Run(async () => await _apiServices.SendGivenAnswerAsync(new Get_API_Url().SendGivenAnswerApi(_baseUrlSendAns), true, objHeaderModel, _objSendGivenAnswerRequestModel.GivenAnswers)).Result;
                    if (StatusCodeClass.StatusCode == "OK")
                    {
                        //_objLoadGivenAnswerResponseModel.LoadAnswers2DB= Task.Run(async () => await _apiServices.GetAsyncData_GetApi(new Get_API_Url().LoadGivenAnswerApi(_baseUrlLoadAns, _surveyId), true, objHeaderModel, _objLoadGivenAnswerResponseModel.LoadAnswers2DB)).Result;
                        //var result = _objLoadGivenAnswerResponseModel.LoadAnswers2DB;
                        //_surveyId = _objPageTypeResPonseModel.Id;
                        //if (_objLoadGivenAnswerResponseModel.LoadAnswers2DB.Count>=0)
                        //{
                        //PageIDE = _objPageTypeResPonseModel.Id;
                        if (Settings._surveyIdListDynamic != null)
                        {
                            Settings._surveyIdListDynamic.Add(PageIDE);
                            await Navigation.PushAsync(new QuestionPageTabOne(null, 1));
                        }
                        //}
                        //else
                        //{
                        //    DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please Attempt the question Again");
                        //}
                    }
                    else
                    {
                        DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please try Again!");
                    }                  
                }
                else
                {
                    DependencyService.Get<IToast>().ShowToast("please Select any suitable option to continue!");
                }               
                await Navigation.PopAllPopupAsync();                                                                             
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                await Navigation.PopAllPopupAsync();
            }          
        }    
        
        private   List<LoadGivenAnswerRequestModel> IsAnsSelected()
        {
            try
            {
                _objLoadGivenAnswerResponseModel.LoadAnswers2DB = Task.Run(async () => await _apiServices.GetAsyncData_GetApi(new Get_API_Url().LoadGivenAnswerApi(_baseUrlLoadAns, PageIDE), true, objHeaderModel, _objLoadGivenAnswerResponseModel.LoadAnswers2DB)).Result;
                _objLoadGivenAnsList = _objLoadGivenAnswerResponseModel.LoadAnswers2DB;
                return _objLoadGivenAnsList;
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
                return _objLoadGivenAnsList;               
            }           
        }
        private async void OnprevbtnPressed(object sender, EventArgs e)
        {
            try
            {
                imgNext.IsVisible = true;
                if (Settings._surveyIdListDynamic != null && Settings._surveyIdListDynamic.Count > 0)
                {
                    Settings._surveyIdListDynamic.RemoveAt(Settings._surveyIdListDynamic.Count - 1);
                }
                if (Settings._surveyIdList != null && Settings._surveyIdList.Count > 0)
                {
                    Settings._surveyIdList.RemoveAt(Settings._surveyIdList.Count - 1);
                    PageIDE = Settings._surveyIdList[Settings._surveyIdList.Count - 1];
                }
                await  Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }
        #endregion
    }
}
