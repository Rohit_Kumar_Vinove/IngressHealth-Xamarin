﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        #region   varibale Declaration

        private string _baseUrl;
        private RestApi _apiServices;
        private SurveyResponseModel _objSurveyResPonseModel=new SurveyResponseModel();
        private SurveyRequestModel _objSurveyRequestModel=new SurveyRequestModel();     
        string[] items = new string[3];

    #endregion
    public MenuPage()
        {
            InitializeComponent();           
            NavigationPage.SetHasNavigationBar(this, false);
            _baseUrl = Settings.Url + Domain.GetSurveyApiConstant; 
            _apiServices = new RestApi();
            LoadPageData();
            Task.Delay(5000);

        }
      
        private async Task LoadDataToxaml()
        {           
            lblmyDignosisright.Text = _objSurveyResPonseModel.GetSurveyList[0].Name;
            lbltabSecond.Text = _objSurveyResPonseModel.GetSurveyList[1].Name;
            lblPtLikeMe.Text = _objSurveyResPonseModel.GetSurveyList[2].Name;
            lblMS_Tagebuch.Text = _objSurveyResPonseModel.GetSurveyList[3].Name;
            await Navigation.PopAllPopupAsync();
        }

        protected override  void OnAppearing()
        {
            // Set up activity indicator        
            LoadDataToxaml();         
            // Remove activity indicator and set up real views
        }
        private  void LoadPageData()
        {
            try
            {
                  HeaderModel objHeaderModel = new HeaderModel();
                objHeaderModel.AccessToken = Settings.Access_token;
               
                _objSurveyResPonseModel.GetSurveyList =  Task.Run(async() =>await _apiServices.GetAsyncData_GetApiList(new Get_API_Url().SurveyApi(_baseUrl), true, objHeaderModel, _objSurveyResPonseModel.GetSurveyList)).Result;
               
               

                  
               
               
             
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
           
        }

       

        private async void myDignosisrightClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new DiagnoseRightMenuPage(_objSurveyResPonseModel.GetSurveyList[0]),true);
        }

        private async void tabSecondbtnClick(object sender, EventArgs e)
        {
            //if(_objSurveyResPonseModel.GetSurveyList[1].Active==true)
            //{
                await Navigation.PushAsync(new TreatmentRightPage(_objSurveyResPonseModel.GetSurveyList[1]), true);
           // }
           // else
           // {
             //   DependencyService.Get<IToast>().ShowToast("Please Buy a Package to See the Survey..");
              //  await Navigation.PushAsync(new SettingPage(), true);
           // }
        }
        private async void tabThirdbtnClick(object sender, EventArgs e)
        {
            if (_objSurveyResPonseModel.GetSurveyList[2].Active == true)
            {
                await Navigation.PushAsync(new PatientLikeMePage(_objSurveyResPonseModel.GetSurveyList[2]), true);
            }
            else
            {
                DependencyService.Get<IToast>().ShowToast("Please Buy a Package to See the Survey..");
                await Navigation.PushAsync(new SettingPage(), true);
            }           
        }
        private async void tabFourthbtnClick(object sender, EventArgs e)
        {
            if (_objSurveyResPonseModel.GetSurveyList[3].Active == true)
            {
                await Navigation.PushAsync(new MY_MS_DiaryPage(_objSurveyResPonseModel.GetSurveyList[3]), true);
            }
            else
            {
                DependencyService.Get<IToast>().ShowToast("Please Buy a Package to See the Survey..");
                await Navigation.PushAsync(new SettingPage(), true);
            }

            
        }
        //private async void lblZU_MS_Click(object sender, EventArgs e)
        //{
        //    await Navigation.PushAsync(new InformationAboutMSPage(),true);
        //}
        private async void lblImpressum_Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ContactUsPage());
        }
        private async void lblFAQ_Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new FAQPage());
        }
       
        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            return true;
        }
      
    }
}
