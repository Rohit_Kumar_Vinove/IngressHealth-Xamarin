﻿using Ingress_Health.Interface;
using Ingress_Health_Api.RequestModel;
using PayPal.Forms;
using PayPal.Forms.Abstractions;
using PayPal.Forms.Abstractions.Enum;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaymentPage : ContentPage
    {
       private ShowPackagetoBuyRequest _objShowPackagetoBuyRequest;
        Double packagePrice;
        public PaymentPage(ShowPackagetoBuyRequest objShowPackagetoBuyRequest)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _objShowPackagetoBuyRequest = objShowPackagetoBuyRequest;
            DisplaySelectedpackage();
        }
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private async void OnLoginPaypalButtonPressed(object sender, EventArgs e)
        {
            try
            {

                //string  Email = lblemailid.Text;
                //  string password = lblpassword.Text;

                //  if (string.IsNullOrWhiteSpace(Email) || string.IsNullOrWhiteSpace(password))
                //  {
                //      await DisplayAlert("Alert", "Please Enter Valid Email or Passowrd!!", "OK");
                //  }
                //  else
                //  {
                //      await DisplayAlert("Alert", "LoginSucessfull!", "OK");

                //  }
                var result = await CrossPayPalManager.Current.Buy(new PayPalItem(lblPackageName.Text, new Decimal(0.0), "EUR"), new Decimal(packagePrice));
                if (result.Status == PayPalStatus.Cancelled)
                {
                    Debug.WriteLine("Cancelled");
                    DependencyService.Get<IToast>().ShowToast(result.Status.ToString());
                }
                else if (result.Status == PayPalStatus.Error)
                {
                    Debug.WriteLine(result.ErrorMessage);
                    DependencyService.Get<IToast>().ShowToast(result.Status.ToString());
                }
                else if (result.Status == PayPalStatus.Successful)
                {
                    Debug.WriteLine(result.ServerResponse.Response.Id);
                    DependencyService.Get<IToast>().ShowToast(result.Status.ToString());
                }
            }
            catch (Exception ex)
            { await DisplayAlert("Alert", "Something Went Wrong please try Again or check your Internet Connection!!", "OK"+ex);
                var message = ex.Message;
            }


        }
        private void DisplaySelectedpackage()
        {
            string[] values = _objShowPackagetoBuyRequest.Description.Split(':');
            lblPackageName.Text = values[1].Replace(@"\", null);
            packagePrice = _objShowPackagetoBuyRequest.Price;
            lblPackagePrice.Text = _objShowPackagetoBuyRequest.Price.ToString()+" €";
        }
        //private void lblemailid_Focused(object sender, FocusEventArgs e)
        //{
        //    emailSuccessErrorImage.IsVisible = true;
        //}

        //private void lblpassword_Focused(object sender, FocusEventArgs e)
        //{
        //    passwordSuccessErrorImage.IsVisible = true;
        //}
    }
}