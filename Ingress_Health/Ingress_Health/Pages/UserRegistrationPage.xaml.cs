﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health.ViewModels;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserRegistrationPage : ContentPage
    {
        #region   varibale Declaration

        private string _baseUrl;
        private RestApi _apiServices;
        private RegisterUserResponseModel _objRegisterUserResPonseModel;
        private RegisterUserRequestModel _objRegisterUserRequestmodel;
        RegisterUserRequestModel obj = new RegisterUserRequestModel();

        #endregion
        public UserRegistrationPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            BindingContext = obj;
            _baseUrl = Settings.Url + Domain.RegisterApiConstant; ;
            _apiServices = new RestApi();

        }

        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }

        private async void btnRegisterUser_Clicked(object sender, EventArgs e)
        {
            try
            {
                _objRegisterUserRequestmodel = obj;

                if (string.IsNullOrWhiteSpace(_objRegisterUserRequestmodel.Email) ||
                    string.IsNullOrWhiteSpace(_objRegisterUserRequestmodel.Password) ||
                    string.IsNullOrWhiteSpace(_objRegisterUserRequestmodel.ConfirmPassword)||
                    string.IsNullOrWhiteSpace(_objRegisterUserRequestmodel.Username))
                {
                    DependencyService.Get<IToast>().ShowToast("Please Enter Valid Credentaials!!..");
                }
                else
                {
                    await Navigation.PushPopupAsync(new LoadingPopPage());
                    _objRegisterUserResPonseModel = await _apiServices.RegisterAsync(new Get_API_Url().LoginApi(_baseUrl), false, new HeaderModel(), _objRegisterUserRequestmodel);

                    if (_objRegisterUserResPonseModel.Status == "Success")
                    {
                        App.IsLoggedIn = true;
                        Settings.Username = _objRegisterUserRequestmodel.Username;
                        Settings.Password = _objRegisterUserRequestmodel.Password;
                        DependencyService.Get<IToast>().ShowToast("Register Sucessfully..");
                        await Navigation.PushAsync(new LoginPage(), true);
                        await Navigation.PopAllPopupAsync();
                    }
                    else
                    {
                        DependencyService.Get<IToast>().ShowToast(_objRegisterUserResPonseModel.ModelState +"Please try with another Email"
                            + "and Your password should Contains at least 1 cap letter 1 small letter 1 special Char and 1 Number!!");
                        await Navigation.PopAllPopupAsync();
                    }
                }

            }
            catch (Exception ex)
            {
                DependencyService.Get<IToast>().ShowToast("Something Went Wrong please try Again or check your Internet Connection!!");
                await Navigation.PopAllPopupAsync();
                var message = ex.Message;
            }
        }

        private void txtEmail_Focused(object sender, FocusEventArgs e)
        {
            try
            {
                emailSuccessErrorImage.IsVisible = true;
            }
            catch(Exception ex)
            {
                var message = ex.Message;
            }
        }

        private void txtPassword_Focused(object sender, FocusEventArgs e)
        {
            try
            {
                DisplayAlert("Info", "Your Password Must Contain 1 Capital Letter 1 small Letter 1 Number and 1 Special Character", "Ok");
                passwordSuccessErrorImage.IsVisible = true;
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }

        private void txtConfirmPassword_Focused(object sender, FocusEventArgs e)
        {
            try
            {
                ConfirmpasswordSuccessError.IsVisible = true;
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }
    }
}