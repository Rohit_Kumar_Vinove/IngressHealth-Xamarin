﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OutPutPageOne : ContentPage
    {
        #region   varibale Declaration

        private string _baseUrl;
        private string _baseUrlNext;
        private int _turn;
        private RestApi _apiServices;
       // private static int _outputSurveyId;
       // private OupPut1NextPageResponseModel _objOupPut1NextPageResponseModel;
        private ResultPageResponseModel _objResultPageResponseModel;
        private HeaderModel _objHeaderModel;
        // private PageTypeResponseModel _objPageTypeResPonseModel;
        // private SurveyRequestModel _objSurveyRequestModel;
        #endregion
        #region Constructor
        public OutPutPageOne(ResultPageResponseModel objResultPageResponseModel, int Turn)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);           
                _objResultPageResponseModel = new ResultPageResponseModel();
            _objResultPageResponseModel = objResultPageResponseModel;
      //_objOupPut1NextPageResponseModel = new OupPut1NextPageResponseModel();
          
            _turn = Turn;
            _apiServices = new RestApi();
            _baseUrl = Settings.Url + Domain.GetResultPageApiConstent;
            _baseUrlNext = Settings.Url + Domain.GetResultNextPageApiConstent;
            _objHeaderModel = new HeaderModel();
            _objHeaderModel.AccessToken = Settings.Access_token;
           
            // LoadpageData();
        }
        #endregion
        #region Events & Methods
        /// <summary>
        /// method for displaying page UI
        /// </summary>
        /// <returns></returns>
        private async void LoadDataToxaml()
        {
          //  await Navigation.PushPopupAsync(new LoadingPopPage());
            lblOutPutOne.Text =_objResultPageResponseModel.Description;
            lblHeadertxt.Text = _objResultPageResponseModel.Header;
            await Navigation.PopAllPopupAsync();
           
        }
        protected override void OnAppearing()
        {
            // Set up activity indicator        
          
            if (_turn == 1)
            {
               
                LoadDataToxaml();
            }
            else
            {
               
                LoadDataToxaml();
            }
            GC.Collect();
            // Remove activity indicator and set up real views
        }
        /// <summary>
        /// when Back Button is Pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        /// <summary>
        ///  clicked event on previous button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
    private void OnprevbtnPressed(object sender, EventArgs e)
    {
        try
        {
                App.Current.MainPage.Navigation.PopAsync();
            }
        catch (Exception ex)
        {
                var message = ex.Message;
            }

    }
        /// <summary>
        /// clicked event next button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
    private async  void OnnextbtnPressed(object sender, EventArgs e)
    {
        try
        {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                LoadpageData(_baseUrlNext, _objResultPageResponseModel.Id);
               // await Navigation.PopAllPopupAsync();
                         
            }
            catch (Exception ex)
        {
                var message = ex.Message;
              await  Navigation.PopAllPopupAsync();
            }

    }
        /// <summary>
        /// Load the page data here
        /// </summary>
        private async void LoadpageData(string Url, long Id)
        {
            try
            {

              //  await Navigation.PushPopupAsync(new LoadingPopPage());
                _objResultPageResponseModel = await _apiServices.GetAsyncData_GetApi(new Get_API_Url().GetAnswerBySurveryIdApi(Url, Id), true, _objHeaderModel, _objResultPageResponseModel);

                if (_objResultPageResponseModel.Id > 0)
                {
                 //   _outputSurveyId = _objResultPageResponseModel.Id;
                    BtnNextPage.IsVisible = true;
                   //await LoadDataToxaml();
                    switch (_objResultPageResponseModel.PageType)
                    {
                        case 6:
                            await Navigation.PushAsync(new OutPutPageOne(_objResultPageResponseModel, 2), true);                       
                            return;
                        case 7:
                            await Navigation.PushAsync(new OutputPageTwo(_objResultPageResponseModel, 2), true);                       
                            return;
                        case 8:
                            await Navigation.PushAsync(new OutputPageThree(_objResultPageResponseModel, 2), true);                        
                            return;
                        default:
                            return;
                    }


                 
                }
                else
                {
                    BtnNextPage.IsVisible = false;
                    DependencyService.Get<IToast>().ShowToast("this was the Last Page!");
                    await Navigation.PopAllPopupAsync();
                }
            }
            catch (Exception ex)
            {
                await Navigation.PopAllPopupAsync();
                var message = ex.Message;
            }
        }

       
        #endregion
    }
}
