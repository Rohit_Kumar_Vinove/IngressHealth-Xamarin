﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OutputPageThree : ContentPage
    {
        #region   varibale Declaration

        private string _baseUrl;
        private string _baseUrlNext;
        private int _turn;
        private RestApi _apiServices;    
        private ResultPageResponseModel _objResultPageResponseModel;
        private HeaderModel _objHeaderModel;
        #endregion
        #region Constructor
        public OutputPageThree(ResultPageResponseModel objResultPageResponseModel, int Turn)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
         
            _turn = Turn;
            _objResultPageResponseModel = new ResultPageResponseModel();
            
            _objResultPageResponseModel = objResultPageResponseModel;
            _apiServices = new RestApi();
            _baseUrlNext= Settings.Url + Domain.GetResultNextPageApiConstent;
            _baseUrl = Settings.Url + Domain.GetResultPageApiConstent;
        
            _objHeaderModel = new HeaderModel();
            _objHeaderModel.AccessToken = Settings.Access_token;
            
         
        }
        #endregion
        #region Methods & Events
        /// <summary>
        /// this method called when UI is appeard
        /// </summary>
        protected override void OnAppearing()
        {
            // Set up activity indicator        
           
            if (_turn == 1)
            {              
                LoadDataToxaml();
            }
            else
            {              
                LoadDataToxaml();
            }
            GC.Collect();
            // Remove activity indicator and set up real views
        }
        /// <summary>
        /// Method to load the Page UI data
        /// </summary>
        /// <returns></returns>
        private async void LoadDataToxaml()
        {
         // await  Navigation.PushPopupAsync(new LoadingPopPage());
            imgFinalOutput.Source = _objResultPageResponseModel.ImageUrl;        
            lbltxtHeaderOutput3.Text = _objResultPageResponseModel.Header;
            await Navigation.PopAllPopupAsync();
            GC.Collect();
        }
        /// <summary>
        /// Clicked event for backbutton 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        /// <summary>
        /// clciked event fired when previous arrow button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnprevbtnPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private async void OnnextbtnPressed(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                LoadpageData(_baseUrlNext, _objResultPageResponseModel.Id);
               // await Navigation.PopAllPopupAsync();
               
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
       

        private async void LoadpageData(string Url, long Id)
        {
            try
            {

              //  await Navigation.PushPopupAsync(new LoadingPopPage());
                _objResultPageResponseModel = await _apiServices.GetAsyncData_GetApi(new Get_API_Url().GetAnswerBySurveryIdApi(Url, Id), true, _objHeaderModel, _objResultPageResponseModel);

                if (_objResultPageResponseModel.Id > 0)
                {
                 //   PageId4Ans2 = _objResultPageResponseModel.Id;
                    BtnNextPage.IsVisible = true;
                    switch (_objResultPageResponseModel.PageType)
                    {
                        case 6:
                            await Navigation.PushAsync(new OutPutPageOne(_objResultPageResponseModel, 2), true);                          
                            return;
                        case 7:
                            await Navigation.PushAsync(new OutputPageTwo(_objResultPageResponseModel, 2), true);                        
                            return;
                        case 8:
                            await Navigation.PushAsync(new OutputPageThree(_objResultPageResponseModel, 2), true);                          
                            return;
                        default:
                            return;

                    }                  
                }
                else
                {
                    BtnNextPage.IsVisible = false;
                    DependencyService.Get<IToast>().ShowToast("This Was the Last Page!");
                    await Navigation.PopAllPopupAsync();
                }
            }
            catch (Exception ex)
            {
                await Navigation.PopAllPopupAsync();
                var message = ex.Message;
            }
        }
        #endregion
    }
}
