﻿
using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OutputPageTwo : ContentPage
    {
        #region   varibale Declaration

        private string _baseUrl;
        private int _turn;
        private string _baseUrlNext;
        private RestApi _apiServices;
      
        private ResultPageResponseModel _objResultPageResponseModel;
        private HeaderModel _objHeaderModel;
        #endregion
        #region Constructor
        public OutputPageTwo(ResultPageResponseModel objResultPageResponseModel, int Turn)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
       
            _turn = Turn;
            _objResultPageResponseModel = new ResultPageResponseModel();
            _objResultPageResponseModel = objResultPageResponseModel;
            _objHeaderModel = new HeaderModel();
            _objHeaderModel.AccessToken = Settings.Access_token;
            _baseUrl = Settings.Url + Domain.GetResultPageApiConstent;
            _baseUrlNext = Settings.Url + Domain.GetResultNextPageApiConstent;
            _apiServices = new RestApi();
           
         
        }
        #endregion
        #region Methods And Events
        /// <summary>
        /// this method is called when UI Loded 
        /// </summary>
        protected override void OnAppearing()
        {
            // Set up activity indicator        
           

            if (_turn == 1)
            {
                
                LoadDataToxaml();
            }

            else
            {
              
                LoadDataToxaml();
            }
            GC.Collect();
            // Remove activity indicator and set up real views
        }
        /// <summary>
        /// Method for displaying Ui of thr particular page 
        /// </summary>
        /// <returns></returns>
        /// 
        private async void LoadDataToxaml()
        {
         //  await Navigation.PushPopupAsync(new LoadingPopPage());
            imgOutpage2.Source = _objResultPageResponseModel.ImageUrl;
            lbltxtpoutput2.Text = _objResultPageResponseModel.Description;
            lblheaderuotput2.Text = _objResultPageResponseModel.Header;
            await Navigation.PopAllPopupAsync();
            GC.Collect();
        }
        /// <summary>
        /// Event fired when back button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        /// <summary>
        /// click event for previous button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnprevbtnPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        /// <summary>
        /// clicked event for next button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnnextbtnPressed(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                LoadpageData(_baseUrlNext, _objResultPageResponseModel.Id);
                //await Navigation.PopAllPopupAsync();                            
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
       


        /// <summary>
        /// Load the page data here
        /// </summary>
        private async void LoadpageData(string Url, long Id)
        {
            try
            {

             //   await Navigation.PushPopupAsync(new LoadingPopPage());
                _objResultPageResponseModel = await _apiServices.GetAsyncData_GetApi(new Get_API_Url().GetAnswerBySurveryIdApi(Url, Id), true, _objHeaderModel, _objResultPageResponseModel);

                if (_objResultPageResponseModel.Id > 0)
                {
                  //  PageId4Ans1 = _objResultPageResponseModel.Id;
                    BtnNextPage.IsVisible = true;                
                    switch (_objResultPageResponseModel.PageType)
                    {
                        case 6:
                            await Navigation.PushAsync(new OutPutPageOne(_objResultPageResponseModel, 2), true);                       
                            return;
                        case 7:
                            await Navigation.PushAsync(new OutputPageTwo(_objResultPageResponseModel, 2), true);                          
                            return;
                        case 8:
                            await Navigation.PushAsync(new OutputPageThree(_objResultPageResponseModel, 2), true);                         
                            return;
                        default:
                            return;

                    }


                   // await Navigation.PopAllPopupAsync();
                }
                else
                {
                    BtnNextPage.IsVisible = false;
                    DependencyService.Get<IToast>().ShowToast("this was the last Page!");
                    await Navigation.PopAllPopupAsync();
                }
            }
            catch (Exception ex)
            {
                await Navigation.PopAllPopupAsync();
                var message = ex.Message;
            }
        }
        #endregion
    }
}
