﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FAQPage : ContentPage
    {
        #region   varibale Declaration

        private string _baseUrl;
        private string _baseUrlFAQ;
        private static int _surveyId;
        private string _baseUrlNextQuestion;
        private RestApi _apiServices;
        HeaderModel _objHeaderModel;
        private glossorResponseModel _ObjglossorResponseModel;
        private GlossorRequestModel _ObjGlossorRequestModel;
        private FAQResponseModel _ObjFAQResponseModel;

        public static List<FAQRequestModel> GetQuestForFaq;
        #endregion
        public FAQPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _baseUrl = Settings.Url + Domain.GetGlossorApiConstant;
            _baseUrlFAQ = Settings.Url + Domain.GetFAQApiConstant;
            _objHeaderModel = new HeaderModel();
            _apiServices = new RestApi();
            _objHeaderModel.AccessToken = Settings.Access_token;
            _ObjglossorResponseModel = new glossorResponseModel();
            _ObjGlossorRequestModel = new GlossorRequestModel();
            _ObjFAQResponseModel = new FAQResponseModel();
            GetQuestForFaq = new List<FAQRequestModel>();
            LoadGlossorList();
            LoadFAQList();
        }
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        protected override void OnAppearing()
        { }
           
        
        private async void LoadGlossorList()
        {
            try
            {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                _ObjglossorResponseModel.GetGlossorList= Task.Run(async () => await _apiServices.GetAsyncData_GetApiList(new Get_API_Url().GlossorApi(_baseUrl), true, _objHeaderModel, _ObjglossorResponseModel.GetGlossorList)).Result;
                if (_ObjglossorResponseModel.GetGlossorList.Count>0)
                {
                    listGlossor.ItemsSource = _ObjglossorResponseModel.GetGlossorList;
                    await Navigation.PopAllPopupAsync();
                }
                else
                {
                    DependencyService.Get<IToast>().ShowToast("Login Sucessfully..");
                    await Navigation.PopAllPopupAsync();
                }
            }
        catch(Exception ex)
            {
                var message = ex.Message;
            }
            }
        private async void LoadFAQList()
        {
            try
            {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                _ObjFAQResponseModel.GetFAQList = Task.Run(async () => await _apiServices.GetAsyncData_GetApiList(new Get_API_Url().FAQApi(_baseUrlFAQ), true, _objHeaderModel, _ObjFAQResponseModel.GetFAQList)).Result;
                foreach(var items in _ObjFAQResponseModel.GetFAQList)
                {
                    items.IsVisible = false;
                }
                if (_ObjglossorResponseModel.GetGlossorList.Count > 0)
                {
                    listFAQ.ItemsSource = _ObjFAQResponseModel.GetFAQList;                                       
                    await Navigation.PopAllPopupAsync();
                }
                else
                {
                    DependencyService.Get<IToast>().ShowToast("Login Sucessfully..");
                    await Navigation.PopAllPopupAsync();
                }
            }
            catch (Exception ex)
            {
                await Navigation.PopAllPopupAsync();
                var message = ex.Message;
            }
        }

        private void listFAQ_ItemTapped(object sender, ItemTappedEventArgs e)
        {         
            var product = e.Item as FAQRequestModel;
            _ObjFAQResponseModel.HideShowPrpduct(product);
        }

       
    }
}