﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MS_DiaryInputPage : ContentPage
    {
        #region variable declaration
        private string _baseUrl;            
        private RestApi _apiServices;
        private HeaderModel _objHeaderModel;
       // List<DiaryEntryValue> DiaryEntrys;
        private SendDiaryEntryResponseModel _objSendDiaryEntryResponseModel;
        private SendDiaryEntryRequestModel _objSendDiaryEntryRequestModel;
        private GetDiaryEntryResponseModel _objGetDiaryEntryResponseModel;
        #endregion
        public MS_DiaryInputPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            lblDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            _apiServices = new RestApi();
            BindingContext = this;
            _baseUrl = Settings.Url + Domain.GetDiaryEntryApiConstent;
            _objHeaderModel = new HeaderModel();
            _objHeaderModel.AccessToken = Settings.Access_token;
            _objGetDiaryEntryResponseModel = new GetDiaryEntryResponseModel();
            _objSendDiaryEntryResponseModel = new SendDiaryEntryResponseModel();
            _objSendDiaryEntryRequestModel = new SendDiaryEntryRequestModel();
           // _objSendDiaryEntryRequestModel.DiaryEntryValues = new List<SendDiaryEntryValue>();
          //  DiaryEntrys = new List<DiaryEntryValue>();
            GetAllDiaryInput();
        }

        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();

            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }

        private async void OnvarlaufPressed(object sender, EventArgs e)
        {
            try
            {
           if(_objSendDiaryEntryRequestModel.DiaryEntryValues.Count>0)
                {
                   
                    _objSendDiaryEntryRequestModel.Id = -1;
                    _objSendDiaryEntryRequestModel.Date= DateTime.Now;
                    _objSendDiaryEntryRequestModel.Header = null;
                    _objSendDiaryEntryRequestModel.PageType = 0;                   
                     await Navigation.PushPopupAsync(new LoadingPopPage());
                    _objSendDiaryEntryResponseModel = Task.Run(async () => await _apiServices.SendDiaryEntryAsync(new Get_API_Url().GetDiaryEntryApi(_baseUrl), true, _objHeaderModel, _objSendDiaryEntryRequestModel)).Result;
                    if (StatusCodeClass.StatusCode == "OK")
                    {
                        await Navigation.PushAsync(new MS_DiaryCalenderPage(), true);
                        await Navigation.PopAllPopupAsync();
                    }
                    else
                    {
                        DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please try again later !");
                        await Navigation.PopAllPopupAsync();
                    }  
                }
                else
                {
                    DependencyService.Get<IToast>().ShowToast("Please select some value first !!");
                }
               

            }
            catch (Exception ex)
            {
                var message = ex.Message;
                await Navigation.PopAllPopupAsync();
            }

        }

        private async void GetAllDiaryInput()
        {
            try
            {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                _objGetDiaryEntryResponseModel = Task.Run(async () => await _apiServices.GetAsyncData_GetApi(new Get_API_Url().GetDiaryEntryApi(_baseUrl), true, _objHeaderModel, _objGetDiaryEntryResponseModel)).Result;
                if (_objGetDiaryEntryResponseModel.Id != 0)
                {
                    listprogressbar.ItemsSource = _objGetDiaryEntryResponseModel.DiaryEntryValues;
                    await Navigation.PopAllPopupAsync();
                }
                else
                {
                    await Navigation.PopAllPopupAsync();
                    DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please try again later !");
                }
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
                await Navigation.PopAllPopupAsync();
            }
        }

        private void slidersymptoms_ValueChanged(object sender, ValueChangedEventArgs e)
        {           
                 var Obj = sender as BindableObject;
                 var SliderSelectedValue = Obj.BindingContext as DiaryEntryValue;           
          
            var existingObj = (from result in _objSendDiaryEntryRequestModel.DiaryEntryValues
                               where result.PageId == SliderSelectedValue.PageId
                               select result).FirstOrDefault();

            if (existingObj == null)
            {
                SendDiaryEntryValue objDiaryEntryValue = new SendDiaryEntryValue();
                objDiaryEntryValue.PageId = SliderSelectedValue.PageId;
                objDiaryEntryValue.Title = SliderSelectedValue.Title;
                objDiaryEntryValue.Value = SliderSelectedValue.Value;
                _objSendDiaryEntryRequestModel.DiaryEntryValues.Add(objDiaryEntryValue);
            }
            else
            {
                existingObj.Value= SliderSelectedValue.Value; 
            }
           
        }

        private void slidersymptoms_Focused(object sender, FocusEventArgs e)
        {           
            e.ToString();
        }
    }
}
