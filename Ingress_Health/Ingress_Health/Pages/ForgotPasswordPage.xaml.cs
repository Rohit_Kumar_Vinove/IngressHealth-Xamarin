﻿
using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPasswordPage : ContentPage
    {
        #region   varibale Declaration

        private string _baseUrl;
        private RestApi _apiServices;
        private ResetPasswordResponseModel _objResetPasswordResponseModel;
        private ResetPasswordRequestModel _objResetPasswordRequestModel;      
        string Email = null;       
        #endregion
        public ForgotPasswordPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _objResetPasswordRequestModel = new ResetPasswordRequestModel();
            _objResetPasswordResponseModel = new ResetPasswordResponseModel();
            _apiServices = new RestApi();
            _baseUrl = Settings.Url + Domain.ResetPasswordApiConstant;
        }
        private async void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                await App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void lblrstEmail_Focused(object sender, FocusEventArgs e)
        {
            emailSuccessErrorImage.IsVisible = true;
        }
        private async void ResetBtnClick(object sender, FocusEventArgs e)
        {
             Email = lblrstEmail.Text;
            if (string.IsNullOrEmpty(Email))
            {
                DependencyService.Get<IToast>().ShowToast("Please Enter Valid Email!!..");
            }
            else
            {
                _objResetPasswordRequestModel.Email = Email;
                _objResetPasswordRequestModel.Password = "";
                _objResetPasswordRequestModel.ConfirmPassword = "";
                await Navigation.PushPopupAsync(new LoadingPopPage());
                _objResetPasswordResponseModel = await _apiServices.ResetPasswordAsync(new Get_API_Url().ResetpasswordApi(_baseUrl), false, new HeaderModel(), _objResetPasswordRequestModel);
                await Navigation.PopAllPopupAsync();
            }
        }
    }
}
