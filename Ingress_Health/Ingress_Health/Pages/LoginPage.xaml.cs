﻿using Ingress_Health.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Rg.Plugins.Popup.Extensions;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.ResponseModel;
using Ingress_Health_Api.RequestModel;
using Ingress_Health.Helpers;
using Ingress_Health.Models;
using Ingress_Health_Api.Model;
using Ingress_Health.Interface;

namespace Ingress_Health.Pages
{
   [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        #region   varibale Declaration

        private string _baseUrl;
        private RestApi _apiServices;
        private LoginResponseModel _objLoginResPonseModel;
        private LoginRequestModel _objLoginrequestmodel;
         LoginRequestModel obj = new LoginRequestModel();
        string Email = null;
        string password = null;
        #endregion

        
        #region Constructor
        public LoginPage()
        {
            InitializeComponent();       
            NavigationPage.SetHasNavigationBar(this, false);          
            BindingContext = obj;
            _baseUrl = Settings.Url + Domain.LoginApiConstant; ;
            _apiServices = new RestApi();
        if(!string.IsNullOrEmpty(Settings.Username)&& !string.IsNullOrEmpty(Settings.Password))
            {
                lblemailid.Text=Settings.Username;
                lblpassword.Text = Settings.Password;
            }
        }
        #endregion

        #region Events
        private async void LoginBtnClick(object sender, EventArgs e)
        {
            try
            {

           _objLoginrequestmodel = obj;
            Email = lblemailid.Text;
            password = lblpassword.Text;
                //    _objLoginrequestmodel.Username = Email;
                //    _objLoginrequestmodel.Password = password;
               // var txtColor = lblemailid.TextColor;
            if (string.IsNullOrWhiteSpace(Email) || string.IsNullOrWhiteSpace(password))
            {
                    DependencyService.Get<IToast>().ShowToast("Please Enter Valid Email or Passowrd!!..");                  
            }           
            else
            {
                 await   Navigation.PushPopupAsync(new LoadingPopPage());
                    _objLoginResPonseModel = await _apiServices.LoginAsync(new Get_API_Url().LoginApi(_baseUrl),false, new HeaderModel(), _objLoginrequestmodel);

                    if (_objLoginResPonseModel.Status== "Success")
                    {
                        Settings.IsLoggedIn = true;
                        Settings.Username = _objLoginResPonseModel.Username;
                        Settings.Password = _objLoginrequestmodel.Password;
                        Settings.Access_token = _objLoginResPonseModel.Access_token;
                        Settings.TokenExpiration = EipirationDate.Expires;
                        DependencyService.Get<IToast>().ShowToast("Login Sucessfully..");
                        await Navigation.PushAsync(new MenuPage(), true);
                        await Navigation.PopAllPopupAsync();
                    }
                    else
                    {
                        DependencyService.Get<IToast>().ShowToast(_objLoginResPonseModel.Error_description);
                        await Navigation.PopAllPopupAsync();
                    }
                }

            }
            catch( Exception ex)
            {
                DependencyService.Get<IToast>().ShowToast("Something Went Wrong please try Again or check your Internet Connection!!");
                await Navigation.PopAllPopupAsync();
                var message = ex.Message;

            }


        }

        private async void forgotPasswordBtnClick(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new ForgotPasswordPage(), true);
            }
            catch (Exception ex){
                var message = ex.Message;
            }
        }

        private async void RegisterUserBtnClick(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new UserRegistrationPage(), true);
            }
            catch (Exception ex)
            {
             await  DisplayAlert("question?","","",""+ex);
                var message = ex.Message;
            }
        }

        private async void FaqlblBtnClick(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new FAQPage(), true);
            }
            catch (Exception ex)
            {
                await DisplayAlert("question?", "", "", "" + ex);
                var message = ex.Message;
            }
        }

        private void lblemailid_Focused(object sender, FocusEventArgs e)
        {
            emailSuccessErrorImage.IsVisible = true;
        }

        private void lblpassword_Focused(object sender, FocusEventArgs e)
        {
            passwordSuccessErrorImage.IsVisible = true;
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            return true;
        }
        #endregion
    }
}
