﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangePasswordPage : ContentPage
    {
        #region Variable Declaration

        string Currentpassword = null;
        string Newpassword = null;
        string ConfirmNewPassword = null;
        private string _baseUrl;
        private RestApi _apiServices;
        HeaderModel _objHeaderModel;
        private ChangePasswordRequestModel _objChangePasswordRequestModel;
        private ChangepasswordResponseModel _objChangepasswordResponseModel;
        #endregion
        public ChangePasswordPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _objChangePasswordRequestModel = new ChangePasswordRequestModel();
            _objChangepasswordResponseModel = new ChangepasswordResponseModel();
            _baseUrl = Settings.Url + Domain.ChangePasswordApiConstant;
            _apiServices = new RestApi();
            _objHeaderModel = new HeaderModel();
            _objHeaderModel.AccessToken = Settings.Access_token;
        }
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void ChangePasswordBtn_Tapped(object sender, EventArgs e)
        {
            try
            {
                Currentpassword = lblCurrentPassword.Text;
                Newpassword = lblnewPassword.Text;
                ConfirmNewPassword = lblConfirmPassword.Text;
                if(!string.IsNullOrEmpty(Currentpassword) && !string.IsNullOrEmpty(Newpassword) && !string.IsNullOrEmpty(ConfirmNewPassword))
                {
                    if(Newpassword==ConfirmNewPassword)
                    {
                        _objChangePasswordRequestModel.OldPassword = Currentpassword;
                        _objChangePasswordRequestModel.NewPassword = Newpassword;
                        _objChangePasswordRequestModel.ConfirmPassword = ConfirmNewPassword;
                        Navigation.PushPopupAsync(new LoadingPopPage());
                        _objChangepasswordResponseModel = Task.Run(async () => await _apiServices.ChangePasswordAsync(new Get_API_Url().ChangepasswordApi(_baseUrl), true, _objHeaderModel, _objChangePasswordRequestModel)).Result;
                        if(StatusCodeClass.StatusCode=="OK")
                        {
                            DependencyService.Get<IToast>().ShowToast("Your Password Has been Changed ..");
                            Settings.Password = ConfirmNewPassword;
                            Navigation.PopAllPopupAsync();
                        }
                        else
                        {
                            DependencyService.Get<IToast>().ShowToast("Error try again !");
                            Navigation.PopAllPopupAsync();
                        }
                    }
                    else
                    {
                        //error not match 
                        DependencyService.Get<IToast>().ShowToast("New Password and Confirm password Not Matched!");
                    }
                }
                else
                {
                    //error empty
                    DependencyService.Get<IToast>().ShowToast("Please Fill out all the field!");
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }

        private void lblCurrentPassword_Focused(object sender, FocusEventArgs e)
        {
            CurrentPasswordSuccessErrorImage.IsVisible = true;
        }

        private void lblnewPassword_Focused(object sender, FocusEventArgs e)
        {
            newPasswordSuccessErrorImage.IsVisible = true;
        }

        private void lblConfirmPassword_Focused(object sender, FocusEventArgs e)
        {
            ConfirmNewpasswordSuccessErrorImage.IsVisible = true;
        }
    }
}