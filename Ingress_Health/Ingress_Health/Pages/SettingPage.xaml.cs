﻿using Ingress_Health.Helpers;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingPage : ContentPage
    {
        #region variable Declaration
        private string _baseUrl;
        private RestApi _apiServices;
        private ShowPackagetoBuyResponse _objShowPackagetoBuyResponse;
        private ShowPackagetoBuyRequest _objShowPackagetoBuyRequest;
        private HeaderModel _objHeaderModel;
        #endregion
        public SettingPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _baseUrl = Settings.Url + Domain.ShowPackage2Buy;
            _apiServices = new RestApi();
            _objShowPackagetoBuyRequest = new ShowPackagetoBuyRequest();
            _objShowPackagetoBuyResponse = new ShowPackagetoBuyResponse();
            _objHeaderModel = new HeaderModel();
            _objHeaderModel.AccessToken = Settings.Access_token;
            lblEmail.Text = Settings.Username;
            lblUsername.Text = Settings.Username;
            lblPassword.Text = Settings.Password;
            Loadpackages();
        }
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private  void OnLogoutButtonPressed(object sender, EventArgs e)
        {
            try
            {
               
                Settings.IsLoggedIn = false;
                Settings.Access_token = string.Empty;
                Navigation.PushAsync(new LoginPage(), true);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void OnIstMeine_ButtonPressed(object sender, EventArgs e)
        {
            try
            {
                // Navigation.PushAsync(new PaymentPage(), true);
                SendSelectedPackage(_objShowPackagetoBuyResponse.GetPackage2Buy[0]);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void OnIstMeinMS_TagebuchButtonPressed(object sender, EventArgs e)
        {
            try
            {
                // Navigation.PushAsync(new PaymentPage(), true);
                SendSelectedPackage(_objShowPackagetoBuyResponse.GetPackage2Buy[1]);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void OnBeides_ButtonPressed(object sender, EventArgs e)
        {
            try
            {
                // Navigation.PushAsync(new PaymentPage(), true);
                SendSelectedPackage(_objShowPackagetoBuyResponse.GetPackage2Buy[2]);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void editemail_Clicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new ChangeEmailPage(), true);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }
        private void editPassword_Tapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new ChangePasswordPage(), true);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }


        private void Loadpackages()
        {
            try {
                //Navigation.PushPopupAsync(new LoadingPopPage());
                _objShowPackagetoBuyResponse.GetPackage2Buy = Task.Run(async () => await _apiServices.GetAsyncData_GetApi(new Get_API_Url().ShowPackage2BuyApi(_baseUrl), true, _objHeaderModel, _objShowPackagetoBuyResponse.GetPackage2Buy)).Result;
            var Result = _objShowPackagetoBuyResponse.GetPackage2Buy;
            if(Result.Count>0)
            {

                     var List1 = Result[0] as ShowPackagetoBuyRequest;                  
                     var List2= Result[1] as ShowPackagetoBuyRequest;
                    var List3 = Result[2] as ShowPackagetoBuyRequest;
                                    
                    string[] values = List1.Description.Split(':');                                    
                    lblPatientenLikeme.Text = values[1].Replace(@"\",null);                                                       
                    lblAptientLikeMePrice.Text = List1.Price.ToString();

                    string[] values1 = List2.Description.Split(':');
                    lblMyDiary.Text = values1[1].Replace(@"\", null); 
                    lblMyDiaryPrice.Text = List2.Price.ToString();

                    string[] values2 = List3.Description.Split(':');
                    lblBoth.Text = values2[1].Replace(@"\", null);
                    lblBothPrice.Text = List3.Price.ToString();
                    // Navigation.PopAllPopupAsync();
                }
            else
            {
               // Navigation.PopAllPopupAsync();
            }
            }
            catch(Exception ex)
            {
                // Navigation.PopAllPopupAsync();
                var message = ex.Message;
            }
        }
        private void SendSelectedPackage(ShowPackagetoBuyRequest _objShowPackagetoBuyRequest)
        {
            try
            {
                Navigation.PushAsync(new PaymentPage(_objShowPackagetoBuyRequest), true);
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }
    }
}
 