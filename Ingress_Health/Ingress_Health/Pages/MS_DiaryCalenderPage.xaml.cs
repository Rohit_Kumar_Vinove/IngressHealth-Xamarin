﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MS_DiaryCalenderPage : ContentPage
    {
        #region variable declaration
        private string _baseUrl;
        private long _symptomsId;
        private string _baseUrlGraph;
        private RestApi _apiServices;
        private HeaderModel _objHeaderModel;
        // List<DiaryEntryValue> DiaryEntrys;
        private AllSymptomsResponseModel _objAllSymptomsResponseModel;
        private GetGraphDataResponseModel _objGetGraphDataResponseModel;
        #endregion

        public MS_DiaryCalenderPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _apiServices = new RestApi();
            _objHeaderModel = new HeaderModel();
            _objHeaderModel.AccessToken = Settings.Access_token;
            _objAllSymptomsResponseModel = new AllSymptomsResponseModel();
            _objGetGraphDataResponseModel = new GetGraphDataResponseModel();
            _baseUrl = Settings.Url + Domain.GetSymptomsListApiConstent;
            _baseUrlGraph = Settings.Url+Domain.GetGraphDataApiConstent;
            GetAllSymptomsList();
        }
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();

            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void OnEintragPressed(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new MS_DiaryInputPage(), true);

            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }

        private async void GetAllSymptomsList()
        {
            try
            {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                _objAllSymptomsResponseModel.GetAllSymptoms = Task.Run(async () => await _apiServices.GetAsyncData_GetApi(new Get_API_Url().GetSymptomsListApi(_baseUrl), true, _objHeaderModel, _objAllSymptomsResponseModel.GetAllSymptoms)).Result;
                var Result = _objAllSymptomsResponseModel.GetAllSymptoms;
                if (Result.Count>0)
                {
                    //await Navigation.PushAsync(new MS_DiaryCalenderPage(), true);
                    PickerSymptomsPickup.ItemsSource = Result;
                    await Navigation.PopAllPopupAsync();
                }
                else
                {
                    DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please try again later !");
                    await Navigation.PopAllPopupAsync();
                }
             
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
            }
        }
      

        private async void PickerSymptomsPickup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var items = PickerSymptomsPickup.SelectedItem as symptoms;
                _symptomsId = items.Id;
                await Navigation.PushPopupAsync(new LoadingPopPage());

                _objGetGraphDataResponseModel.GraphDataList = Task.Run(async () => await _apiServices.GetAsyncData_GetApiList(new Get_API_Url().GetGraphDataApi(_baseUrlGraph, 113), true, _objHeaderModel, _objGetGraphDataResponseModel.GraphDataList)).Result;
                var Result = _objGetGraphDataResponseModel.GraphDataList;
                var list1 = Result[0] as graphdata;
                var list2= Result[1] as graphdata;
                var list3= Result[2] as graphdata;
                if (Result.Count > 0)
                {
                    LineChartSymptoms.ItemsSource = list1.Values;                  
                    LineChartSymptoms.XBindingPath = "Date";
                    LineChartSymptoms.YBindingPath = "Value";

                    LineChartSymptoms1.ItemsSource = list2.Values;
                    LineChartSymptoms1.XBindingPath = "Date";
                    LineChartSymptoms1.YBindingPath = "Value";

                    LineChartSymptoms2.ItemsSource = list3.Values;
                    LineChartSymptoms2.XBindingPath = "Date";
                    LineChartSymptoms2.YBindingPath = "Value";

                    await Navigation.PopAllPopupAsync();
                }
                else
                {
                    DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please try again later !");
                    await Navigation.PopAllPopupAsync();
                }
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
            }
        }
    }
}
