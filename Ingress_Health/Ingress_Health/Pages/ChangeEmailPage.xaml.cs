﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangeEmailPage : ContentPage
    {
        #region Variable Declaration
        string CurrentEmail = null;
        string NewEmail = null;
        private string _baseUrl;
        private RestApi _apiServices;
        HeaderModel _objHeaderModel;
        private ChangeEmailRequestModel _objChangeEmailRequestModel;
        private ChangeEmailResponseModel _objChangeEmailResponseModel;
        #endregion
        #region Constructor
        public ChangeEmailPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _objChangeEmailRequestModel = new ChangeEmailRequestModel();
            _objChangeEmailResponseModel = new ChangeEmailResponseModel();
            _baseUrl = Settings.Url + Domain.ChangeEmailApiConstant;            
            _objHeaderModel = new HeaderModel();
            _apiServices = new RestApi();
            _objHeaderModel.AccessToken = Settings.Access_token;
        }
        #endregion
        #region Events & Method
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void ChangeEmailBtn_Tapped(object sender, EventArgs e)
        {
            CurrentEmail = lblemailid.Text;
            NewEmail = lblnewemailid.Text;

            if(!string.IsNullOrEmpty(CurrentEmail) && !string.IsNullOrEmpty(NewEmail))
            {
                if(CurrentEmail != NewEmail)
                {
                    _objChangeEmailRequestModel.Current = CurrentEmail;
                    _objChangeEmailRequestModel.New = NewEmail;
                     Navigation.PushPopupAsync(new LoadingPopPage());
                    _objChangeEmailResponseModel = Task.Run(async () => await _apiServices.ChangeEmailAsync(new Get_API_Url().ChangeEmailApi(_baseUrl), true, _objHeaderModel, _objChangeEmailRequestModel)).Result;
                    Navigation.PopAllPopupAsync();
                }
                else
                {
                    //Error emails are same
                    DependencyService.Get<IToast>().ShowToast("Both Email are same please enter valid and different email..!");
                }
            }
            else
            {
                //empty
                DependencyService.Get<IToast>().ShowToast("Please fill both the field first!!");
            }        
        }

        private void lblemailid_Focused(object sender, FocusEventArgs e)
        {
            emailSuccessErrorImage.IsVisible = true;
        }

        private void lblnewemailid_Focused(object sender, FocusEventArgs e)
        {
            newemailSuccessErrorImage.IsVisible = true;
        }
        #endregion
    }
}