﻿using Ingress_Health.Helpers;
using Ingress_Health.Interface;
using Ingress_Health.Models;
using Ingress_Health.Pages.QuestionsPages;
using Ingress_Health_Api.ApiHandler;
using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using Ingress_Health_Api.ResponseModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DiagnoseRightMenuPage : ContentPage
    {
        #region   varibale Declaration

        private string _baseUrl;       
        private RestApi _apiServices;
        private ResultPageResponseModel _objResultPageResponseModel;
        private HeaderModel _objHeaderModel;
        private PageTypeResponseModel _objPageTypeResPonseModel;
        private SurveyRequestModel _objSurveyRequestModel;
        #endregion
        public DiagnoseRightMenuPage(SurveyRequestModel objSurveyRequestModel)
        {
            _objSurveyRequestModel = objSurveyRequestModel;
            InitializeComponent();
            _baseUrl = Settings.Url + Domain.GetResultPageApiConstent;     
            _apiServices = new RestApi();
            QuestionPageTabOne._resultpageId = _objSurveyRequestModel.Id;
            _objHeaderModel = new HeaderModel();
            _objHeaderModel.AccessToken = Settings.Access_token;
            _objResultPageResponseModel = new ResultPageResponseModel();
            NavigationPage.SetHasNavigationBar(this, false);
            DesCriptionTexttabOne.Text = _objSurveyRequestModel.Description;
        }
        private async void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
             await   App.Current.MainPage.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private async void OnErgebnissePressed(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                _objResultPageResponseModel = await _apiServices.GetAsyncData_GetApi(new Get_API_Url().GetAnswerBySurveryIdApi(_baseUrl, _objSurveyRequestModel.Id),true, _objHeaderModel, _objResultPageResponseModel);
               
                if (_objResultPageResponseModel.Id>0)
                {
                    switch(_objResultPageResponseModel.PageType)
                    {
                        case 6:
                            await Navigation.PushAsync(new OutPutPageOne(_objResultPageResponseModel, 1), true);
                            await Navigation.PopAllPopupAsync();
                            return;
                        case 7:
                            await Navigation.PushAsync(new OutputPageTwo(_objResultPageResponseModel, 1), true);
                            await Navigation.PopAllPopupAsync();
                            return;
                        case 8:
                            await Navigation.PushAsync(new OutputPageThree(_objResultPageResponseModel, 1), true);
                            await Navigation.PopAllPopupAsync();
                            return;
                        default:
                            return;
                          
                    }
                }
                else
                {
                    DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please try Again!");
                    await Navigation.PopAllPopupAsync();
                }
               
            }
            catch (Exception ex)
            {
                await Navigation.PopAllPopupAsync();
                var message = ex.Message;
            }
        }
        private async void OnFragebogenPressed(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushPopupAsync(new LoadingPopPage());
                //    // _objPageTypeRequestmodel.Accesstoken = Settings.Access_token;
                //    //  _objLoginModel = await _restObj.GetAsyncData_GetApi(new Get_API_Url().LoginApi(_baseUrl, UserName.Text.Trim(), Password.Text.Trim(), "123456"), false, new HeaderModel(), _objLoginModel);
                //    HeaderModel objHeaderModel = new HeaderModel();
                //    objHeaderModel.AccessToken= Settings.Access_token; 
                //    _objPageTypeResPonseModel = await _apiServices.GetAsyncData_GetApi(new Get_API_Url().BtnFrageBogenApi(_baseUrl, _surveyId), true, objHeaderModel, _objPageTypeResPonseModel);

                //    if(_objPageTypeResPonseModel.Id!=0)
                //    {
                Settings._surveyIdList = new List<int>();
                Settings._surveyIdListDynamic = new List<int>();
                await Navigation.PushAsync(new QuestionPageTabOne(_objSurveyRequestModel,0), true);
                  //  await Navigation.PopAllPopupAsync();
               // }
             //   else
             //   {
               //     DependencyService.Get<IToast>().ShowToast("Something Went Wrong Please try Again!");
                    await Navigation.PopAllPopupAsync();
              //  }
                                
            }
            catch (Exception ex)
            {
                await Navigation.PopAllPopupAsync();
                var message = ex.Message;
            }           
        }
    }
}
