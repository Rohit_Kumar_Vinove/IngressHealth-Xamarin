﻿using Ingress_Health_Api.Model;
using Ingress_Health_Api.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ingress_Health.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MY_MS_DiaryPage : ContentPage
    {
        private SurveyRequestModel _objSurveyRequestModel;
        public MY_MS_DiaryPage(SurveyRequestModel _objPageTypeResPonseModel)
        {
            InitializeComponent();
            _objSurveyRequestModel = _objPageTypeResPonseModel;
            NavigationPage.SetHasNavigationBar(this, false);
            lblMyMsDiary.Text = _objSurveyRequestModel.Description;
        }
        private void OnBackButtonPressed(object sender, EventArgs e)
        {
            try
            {
                App.Current.MainPage.Navigation.PopAsync();
               
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void OnEintragPressed(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new MS_DiaryInputPage(),true);

            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
        private void OnvarlaufPressed(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new MS_DiaryCalenderPage(), true);

            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
    }
}
