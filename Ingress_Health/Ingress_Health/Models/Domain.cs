﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health.Models
{
  public static class Domain
    {
        public static string Url
        {
          
            get
            {
                return "https://msapp.into-medpharm.com/";
            }
        }

        public static string LoginApiConstant
        {
            get
            {
                return "token";
            }
        }
        public static string RegisterApiConstant
        {
            get
            {
                return "api/account/register";
            }
        }
        public static string GetPageTypeApiConstant
        {
            get
            {
                return "api/PageTypes";
            }
        }
        public static string GetSurveyApiConstant
        {
            get
            {
                return "api/Survey";
            }
        }

        public static string GetbtnFrageBogenApiConstant
        {
            get
            {
                return "api/Page/";
            }
        }

        public static string GetAnswerOptionsApiConstant
        {
            get
            {
                return "api/answer/";
            }
        }
        public static string GetNextPageQuestionApiConstant
        {
            get
            {
                return "api/NextPage/";
            }
        }
        public static string SaveGivenAnswerApiConstant
        {
            get
            {
                return "api/Answer/";
            }
        }
        public static string GetGlossorApiConstant
        {
            get
            {
                return "api/Glossary";
            }
        }
        public static string GetFAQApiConstant
        {
            get
            {
                return "api/Faq";
            }
        }
        public static string ResetPasswordApiConstant
        {
            get
            {
                return "api/Account/ResetPassword";
            }
        }
        public static string ChangePasswordApiConstant
        {
            get
            {
                return "api/account/ChangePassword";
            }
        }
        public static string ChangeEmailApiConstant
        {
            get
            {
                return "api/Email/";
            }
        }
        public static string ShowPackage2Buy
        {
            get
            {
                return "api/Package";
            }
        }
        public static string GetResultPageApiConstent
        {
            get
            {
                return "api/ResultPage/";
            }
        }
        public static string GetResultNextPageApiConstent
        {
            get
            {
                return "api/NextResultPage/";
            }
        }
        public static string GetDiaryEntryApiConstent
        {
            get
            {
                return "/api/Diary/";
            }
        }
        public static string GetSymptomsListApiConstent
        {
            get
            {
                return "/api/Symptom/";
            }
        }
        public static string GetGraphDataApiConstent
        {
            get
            {
                return "/api/Symptom/";
            }
        }

        public static string SendGivenAnswerApiConstent
        {
            get
            {
                return "/api/Answer/";
            }
        }

        public static string LoadGivenAnswerApiConstent
        {
            get
            {
                return "/api/AnswerGiven/";
            }
        }
    }
}
