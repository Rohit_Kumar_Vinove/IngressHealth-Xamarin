﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ingress_Health.Interface
{
   public interface IToast

    {
        void ShowToast(string message);
    }
}
