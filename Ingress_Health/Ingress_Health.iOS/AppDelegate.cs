﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using AsNum.XFControls.iOS;
using PayPal.Forms.Abstractions;
using PayPal.Forms.Abstractions.Enum;
using Syncfusion.SfChart.XForms.iOS.Renderers;

namespace Ingress_Health.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
                        SfChartRenderer.Init();
            PayPal.Forms.CrossPayPalManager.Init(
                new PayPalConfiguration(
                    PayPalEnvironment.NoNetwork,
                    "Aan8SJnGc__DQNN2gxtyKHM5HfDVGbn2kgDoWREkCSDktYuPyI7HP-y9vENV0QSbsft2oSp9iZ6r2c17"
                )
                {
                    AcceptCreditCards = false,
                    MerchantName = "Test Store",
                    MerchantPrivacyPolicyUri = "https://www.example.com/privacy",
                    MerchantUserAgreementUri = "https://www.example.com/legal",
                    // OPTIONAL - ShippingAddressOption (Both, None, PayPal, Provided)
                  //  ShippingAddressOption = ShippingAddressOption.Both,
                    // OPTIONAL - Language: Default languege for PayPal Plug-In
                    Language = "en",
                   // OPTIONAL - PhoneCountryCode: Default phone country code for PayPal Plug-In
                    PhoneCountryCode = "91"
                }
            );
            AsNumAssemblyHelper.HoldAssembly();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
    }
}
